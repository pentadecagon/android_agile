#include <fstream>
#include <infodir/xlog.hh>
#include "node.hh"
#include <tensor/tensor.hh>

using namespace std;

DataSet read_data( const string& fname );
void verify_nid( const uint32_t nid, RStore& rstore );
void write_nid( const uint32_t nid, WStore& wstore );

double global_write_uint32( const uint32_t a, WStore& wstore );
uint32_t global_read_uint32( RStore& rstore );
double global_cost_uint32( uint32_t a );
double global_write_double( const double a, WStore& wstore );
double global_read_double( RStore& rstore );
double global_cost_double_10000( double d );
double global_cost_double( double d );
void test_store();


