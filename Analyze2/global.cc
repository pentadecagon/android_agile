#include "analyze2.hh"
#include <xutil/statistics.hh>
#include <random>

static const uint32_t nid_global_uint32 = create_nid( );
static const uint32_t nid_global_double = create_nid( );
static const uint32_t nid_global_double10000 = create_nid( );



double global_write_uint32( const uint32_t a, WStore& wstore )
{
	write_nid( nid_global_uint32, wstore );
	double cost = unsigned_cost( a );
	wstore.dump32( a, cost );
	return cost;
}

uint32_t global_read_uint32( RStore& rstore )
{
	verify_nid( nid_global_uint32, rstore );
	return rstore.read32();
}

double global_write_double( const double a, WStore& wstore )
{
	write_nid( nid_global_double, wstore );
	const double cost = 1 + unsigned_cost( abs( lround( a * 10000 )), 10000 );
	wstore.dump64t( a, cost );
	return cost;
}

double global_read_double( RStore& rstore )
{
	verify_nid( nid_global_double, rstore );
	return rstore.read64t<double>();
}

void test_store()
{
	static std::mt19937 eng32;
	const int N = 100;
	vector<double> v1a( N );
	vector<uint32_t> v2a( N );

	WStore wstore;

	for (double& x : v1a){
		x = (int)eng32() / 3 * 0.0001;
		global_write_double( x, wstore );
	}
	for ( uint32_t & x : v2a){
		x = eng32( ) / 4;
		global_write_uint32( x, wstore );
	}
	RStore rstore( wstore );
	for (double x : v1a){
		rassert( x == global_read_double( rstore ) );
	}
	for ( uint32_t x: v2a ){
		rassert( x == global_read_uint32( rstore ) );
	}
	LOGI( "test store finished" );
}

void GaussCodecDouble::add( double a ){
	qsum_ += a*a;
	asum_ += a;
	count_ += 1;
}

void GaussCodecDouble::optimize(){
	mean_ = asum_ / count_;
	sigma_ = sqrt( qsum_ / count_ - sqr( mean_ ) );
	LOGI( "sigma=" << sigma_ << "  mean=" << mean_ );
	recalc_fixcost_and_hash();
}

void GaussCodecDouble::recalc_fixcost_and_hash() {
	rassert( sigma_ > resolution_ * 10 );
	fixcost_ = 0.5 * log2( sqr( sigma_ / resolution_ ) * 2 * M_PI );
	hash_ = lround( 1013 * log( sigma_ ) + 177 * log( resolution_ ) );
}

double GaussCodecDouble::calc_cost( double a ) const {
	return fixcost_ + sqr( (a - mean_) / sigma_ ) * 0.5 * M_LOG2E;
}

double GaussCodecDouble::write( double a, WStore& ws ){
	double cost = calc_cost( a );
	write_nid( hash_, ws );
	ws.dump64t( a, cost );
	return cost;
}

double GaussCodecDouble::read( RStore& rs ){
	verify_nid( hash_, rs );
	return rs.read64t<double>();
}

