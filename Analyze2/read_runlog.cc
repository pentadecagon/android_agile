#include "analyze2.hh"
#include <fstream>
#include <infodir/xlog.hh>

using namespace std;

typedef pair<string, double> SDPair;

template<class T>
pair<string, T> readpair( istream& ist )
{
	pair<string, T> res;
	ist >> ws;
	getline( ist, res.first, '=' );
	ist >> res.second;
	return res;
}

double read_double( istream& ist, char* stub )
{
	SDPair a = readpair<double>( ist );
	rassert( a.first == stub );
	return a.second;
}

DataSet read_data( const string& fname )
{
	ifstream ist( fname.c_str() );
	vector<GyNode> vnode;
	while (1){
		const SDPair a = readpair<double>( ist );
		if (!ist.good()) break;
		if (a.first == "tgy"){
			const double gy = read_double( ist, "gy" );
			vnode.push_back( GyNode( a.second, gy ) );
		} /*else if (a.first == "tacc"){
			const double y = read_double( ist, "yacc" );
			const double z = read_double( ist, "zacc" );
			vnode.push_back( AccNode::create( a.first, y, z ) );
		}*/
		if (vnode.size() > 10000)
			break;
	}
	LOGI( " read: " << vnode.size() );
	DataSet res = { Tensor<GyNode, T1d>( vnode.size(), &vnode[0] ) };
	return res;
}
/*
When creating something new, we have to add it to the list of things to be saved,
and we have to remove other things from that list, and we have to modify the save method of yet other things. 
Our goal is to recreate dataset in a compressed manner.  So we need to load things from the store like this:
- Read bootstrapper.  That would remain unchanged and counts as static.  This bootstrapper reads the main segment. 
This main segment reader contains some kind of description, a list 
*/