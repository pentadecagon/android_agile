#pragma once
#include "codec.hh"
#include <vector>

struct DataSet {
	const Tensor<GyNode, T1d> GyVector;
};

class DoubleNode : public Node {
private:
	explicit DoubleNode( double x ) : a( x ){}
public:
	const double a;
	static DoubleNode* create( double x ){
		return new DoubleNode( x );
	}
};

typedef Ref<DoubleNode> DoubleRef;

struct GyNode  {
	GyNode( double t0, double gy0 ) : t( t0 ), gy( gy0 ){}

	const double t;
	const double gy;
	void xcomp( const GyNode& b ) const {
		rassert( t == b.t && gy == b.gy );
	}
};

