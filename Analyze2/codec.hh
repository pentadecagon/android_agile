#pragma once
#include <stdint.h>
#include <vector>
#include <xutil/rcbase.hh>
#include <string>
#include <infodir/xlog.hh>
using namespace std;

uint32_t create_nid();

struct Node : RCBase {
	Node() = default;
	Node( const Node& ) = delete;
	Node& operator=(const Node&) = delete;
};

typedef Ref<Node> NodeRef;


class WStore {
	vector<uint32_t> v_;
	friend class RStore;
public:
	double total_cost = 0.0;

	void dump32( const uint32_t t, double c ){
		total_cost += c;
		v_.push_back( t );
	}
	template<class T>
	void dump32t( const T& t, double c ){
		total_cost += c;
		static_assert(sizeof(T) == 4, "bad error" );
		v_.push_back( *(uint32_t*)&t );
	}
	template<class T>
	void dump64t( const T& t, double c ){
		total_cost += c;
		static_assert(sizeof(T) == 8, "bad error");
		const int k = v_.size();
		v_.resize( k + 2 );
		*(T*)&v_[k] = t;
	}
};

class RStore {
public:
	RStore( WStore& wstore ) : v_( wstore.v_ ){
		LOGI( "cost: " << wstore.total_cost );
	}
	uint32_t read32( ) {
		rassert( curpos < v_.size( ) );
		return v_[curpos++];
	}
	template<class T>
	T read32t(){
		rassert( curpos  < v_.size() );
		static_assert(sizeof(T) == 4, "bad");
		return *(const T*)&v_[curpos++];
	}
	template<class T>
	T read64t(){
		static_assert( sizeof(T) == 8, "bad" );
		const int k = curpos;
		curpos += 2;
		rassert( curpos <= v_.size() );
		return *(const T*)&v_[k];
	}
	size_t curpos = 0;
	const vector<uint32_t>& v_;
};

class GaussCodecDouble { 
public:
	GaussCodecDouble( double resolution0, double mean0, int sigma0 )
		: resolution_( resolution0 ), mean_( mean0 ), sigma_( sigma0 ) {
		recalc_fixcost_and_hash();
	}
	void add( double );
	double write( double, WStore& );
	double calc_cost( double a ) const;
	double read( RStore& );
	void optimize();
private:
	uint32_t calc_hash() const;
	void recalc_fixcost_and_hash();
	 
	const double resolution_;
	double mean_;
	double sigma_;
	double qsum_ = 0;
	double asum_ = 0;
	int count_ = 0;
	double fixcost_;
	uint32_t hash_;
};

