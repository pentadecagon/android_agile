#include "analyze2.hh"

// global data

GaussCodecDouble gcodec_gy( 1e-4, 0, 1.0 );
GaussCodecDouble gcodec_gyt( 1e-4, 0, 300 );


void verify_nid( const uint32_t nid, RStore& rstore )
{
	const uint32_t h = rstore.read32( );
	rassert( h == nid );
}

void write_nid( const uint32_t nid, WStore& wstore )
{
	wstore.dump32( nid, 0 );
}

uint32_t create_nid()
{
	static uint32_t k = 1;
	k += 1;
	return k;
}

class Store0 {
	Store0( const DataSet& ds ) : ds_( ds ){}

};

/*
We basically start with a certain structure, and add stuff on top of it.  The crux is to find *consistent* subsets of the toplist.
The way to go would be to always name alternatives explicitely, so here we have *either* this or *that* at a specific point.
What about those *set of straights* thing?  
*/