package com.anofax.chargecontrol;

import java.io.BufferedReader;
import java.io.FileReader;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ToggleButton;

public class MainActivity extends Activity {
	static String FileName="/sys/devices/platform/msm_ssbi.0/pm8921-core/pm8921-charger/charge";
	int ReadStatus(){
		try {
			BufferedReader br = new BufferedReader(new FileReader(FileName));
		    int c = br.read();
		    br.close();
		    Log.i("Charger", "charge="+c );
		    return c-48;
		} catch ( Exception e ){
			e.printStackTrace();
			return -1;
		}
	}

	int SetCharge(String s) {
		try {
			String[] command = {"su", "-c",  "echo " + s + " > " + FileName};
			Log.i("Charger", "command="+command[2] );
			Process process = Runtime.getRuntime().exec( command );
			
			int retval = process.waitFor();
			Log.i("Charger", "su returns: " + retval);
			return retval;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ToggleButton toggle = (ToggleButton) findViewById(R.id.toggleButton1);
		toggle.setChecked( ReadStatus() != 0 );
	}
	public void ButtonClicked( View view )
	{
		ToggleButton toggle = (ToggleButton) view;
		if( toggle.isChecked() ){
			SetCharge("1");
		} else {
			SetCharge("0");
		}
		int status = ReadStatus();
		toggle.setChecked( status != 0 );
	}
}
