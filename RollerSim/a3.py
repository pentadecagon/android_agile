import os, math
from collections import defaultdict
#import jpype
from numpy import array
import matplotlib.pyplot as plt
import scipy.optimize as scopt

#jvmPath = jpype.getDefaultJVMPath()
#classpath=r"D:/android/git/android_agile/Roller1/bin/classes"
#jpype.startJVM(jvmPath, '-Djava.class.path=%s' % classpath)
#RunState = jpype.JPackage('tk').rollerrun.RunState
#p=RunState.newParams()
#s=RunState(p)
#print(p)
#print dir(p)
#jpype.java.lang.System.out.println ('Berlusconi likes women')
#s=jpype.java.tk.rollerrun.RunState.newState()


# The thing is, when we read the logfile, we need it for two purposes:
# 1. Plot it.
# 2. Run the simulation.
# The required data structures are wildly different, so we create both of them while reading the thing.


class Bunch:
    def __init__(self, **kwds):
        self.__dict__.update(kwds)

nfig=0
def view(yset1=[], yset2=[], xlabel='', ylabel1='', ylabel2=''):
    global nfig
    fig = plt.figure(nfig)
    nfig += 1
    fig.subplots_adjust( left=0.1, right=0.95, bottom=0.1, top=0.95)
    ax1 = fig.add_subplot(111)
    ax1.set_xlabel(xlabel)
    labels = []
    for (t, x, l) in yset1:
        ax1.plot(t, x, '.-')
        labels.append(l)
    ax1.set_ylabel(ylabel1, color='b')
    
    if yset2:
        ax2 = ax1.twinx()

    plt.legend(labels)

def executeLog( params, logstates ):
    rs=RunState(params)
    t0=0
    philist=[]
    phidotlist=[]
    glist=[]
    for x in logstates:
        x.execute(rs)
        if rs.last_gyro_t != t0:
            philist.append( rs._x.a0)
            phidotlist.append(rs._x.a1)
            glist.append(math.sqrt(rs._P.a00*49+rs._P.a01*14+rs._P.a11))
#            print rs._P.a00
            t0=rs.last_gyro_t
#            print rs._P.det()
    return array(glist), array(phidotlist), rs._totalCost

def calcCost( params, logstates ):
    rs=RunState(params)
    for x in logstates:
        x.execute(rs)
    if math.isnan( rs._totalCost):
        return 10000.0;
    return rs._totalCost

def readLogFile( fname ):
    pairs=open(fname).read().split()
#    params = RunState.newParams()
    datavecs=defaultdict(list)
    run=[]
    def getvals( *a ):
        return tuple( datavecs[x][-1] for x in a )
    
    for x0 in pairs:
        u, v0 = x0.split('=')
        try:
            v=int(v0)
        except:
            v=float(v0)
        if u.startswith("param_"):
            pname=u.replace("param_", "" )
#            setattr( params, pname, v )
            continue
        
        datavecs[u].append(v)

#        if u == 'mode':
#            run.append( RunState.newArdState( *getvals( 'tard', 'steps', 'speed', 'pwm', 'volt', 'mode') ))
#        elif u == 'gy':
#            run.append( RunState.newLogGyro( *getvals( 'tgy', 'gy')))
#        elif u=='zacc':
#            run.append( RunState.newLogZacc( *getvals( 'tzacc', 'zacc' )))
#            if datavecs['tzacc'][-1] > 500 and abs(v) > 3:
#                break
            
            
#    print rs.gx
    print(list(datavecs.keys()))
#    p1, p2, cost = executeLog( params, run )
#    for x, y in zip(p2, datavecs['phidot']):
#        if x != y:
#            print x, y
    adict = dict( (x, array(y)) for (x,y) in list(datavecs.items()) )
    return Bunch(**adict), run
  
def showdatavec(run, p1):
    view( [
           (run.tgy, run.gy*100, "gy*100"),
           (run.tstate, run.phi*100, "phi*100"),
           (run.tard, run.steps, "steps"),
           (run.tzacc, (run.zacc-1.8)*30, "acc*30"),
           (run.tard, run.pwm*0.1, "pwm"),
           (run.tard, run.speed, "speed"),
#           (run.tstate, p1*50, "delta*50"),
#           (run.tstate, (run.phi*7+run.gy)*50, "phi+gy"),
        ])
    
def readAll():
    dirname=r'D:\android\git\android_agile\Scripts/runs/'
    runs=[]
    total_cost=0
    for x in sorted( os.listdir(dirname)):
        if x>"2013-11-04_20.06":
            print("reading: ", x )
            v, run=readLogFile( dirname+x )
            if v.volt[0] < 7:
                continue
            showdatavec(v, None)
#    inner2( params, runs)
#    optimize_parameters(params, runs)
    print("cost: ", total_cost)
  
hhpar = ('noise_v', 'noise_pos', 'noise_gyro', 'alpha');

def params2x(p):
    return array( [getattr(p,x) for x in hhpar])

def x2params(p,avec):
    for (name, val) in zip( hhpar, avec ):
        setattr( p, name, val )
    return p
    
def inner2( params, runs ):    
    erg = 0
    for run in runs:
        erg += calcCost( params, run )
    return erg
    
    
def f_inner( x, params0, events ):

#    assert len(x) == 2
    params = x2params(params0, x )
    
    erg = inner2(params, events)
    print("x=", x, " error: ", erg)
    return erg


def callback(x):
    print(( "Callback: ", x ))

def optimize_parameters(params, runs):
    erg = scopt.fmin(func=f_inner,
                           x0 = params2x(params),
#                           fprime=None,
                           args=(params, runs),
                           xtol=1e-4,
                           ftol=1e-8,
#                           epsilon=1e-4,
#                           maxiter=None,
                           full_output=1, disp=1, retall=0, callback=callback)
    print(erg)
  
  
def main():
    readAll()
    plt.show()
    jpype.shutdownJVM()
    
main()    
            
