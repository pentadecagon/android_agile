APP_ABI := armeabi-v7a
APP_PLATFORM := android-18
NDK_TOOLCHAIN_VERSION := 4.8
APP_STL := gnustl_static
APP_CFLAGS := -std=c++11 -g -gdwarf-2  -mfpu=neon-vfpv4 -frtti

APP_OPTIM := debug
