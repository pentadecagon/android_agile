#include "runstate.hh"
#include <xutil/statistics.hh>
#include <xutil/pyplot.hh>
#include <xutil/statistics.hh>
#include <xutil/passfilter.hh>

RunLog::RunLog( time_t start_time, const string& fname0 )
	: fname_(fname0),
//	  startspeed_(startspeed),
	  start_time_( start_time )
{
}

const RunEventGy*  RunLog::add_gy( double t, float a )
{
	auto res = new RunEventGy( t, a );
	v_.push_back( res );
	return res;
}

const RunEventAcc*  RunLog::add_acc( double t, float a, float b )
{
	auto res = new RunEventAcc( t, a, b );
	v_.push_back( res );
	return res;
}

const RunEventArd* RunLog::add_ard( double t, float steps, float speed, int pwm, float volt, int mode )
{
	auto res = new RunEventArd( t, steps, speed, pwm, volt, mode );
	v_.push_back( res );
	return res;
}

const RunEventBal* RunLog::add_bal( double t, float bal )
{
	auto res = new RunEventBal( t, bal );
	v_.push_back( res );
	return res;
}

RunLog::~RunLog( )
{
	for( RunEvent* x : v_ ) delete x;
}

template<class T>
T readval (istream& ist, const char* stub)
{
	T v;
	string s;
	ist >> ws;
	getline(ist, s, '=');
	rassert(s == stub);
	ist >> v;
	rassert( ist.good( ) );
	ist >> ws;
	return v;
}

const RunLog* load_runlog( const string& fname )
{
	ifstream ist( fname );
	ist >> ws;
	auto readint = [&](const char* stub){ return readval<int>(ist, stub);  };
	auto readfloat = [&](const char* stub){ return readval<float>(ist, stub);  };
	RunLog * rs = new RunLog(0, fname);
	float steplog = 0;
	float lastbalance = 0;
	float balance_to_add = 0;
	float laststeps = 0;
	vector<pair<double, double>>vbal;
	VPDD vgy;
	VPDD vzacc;
	LowPass<float> lpgy( 0.25 );
	LowPass<float> lpzacc( 0.25 );

	while (1){
		if (ist.eof())
			break;
		string timestring;
		getline( ist, timestring, '=' );
		if (timestring == "the end\n")
			break;
		double t;
		ist >> t;
		rassert( ist.good() );
		ist >> ws;
		if (ist.eof()){
			break;
		} else if (timestring == "tgy"){
			float h = readfloat( "gy" );
			vgy.push_back( make_pair( t, lpgy( h ) ) );
			rs->add_gy( t, h );
		} else if (timestring == "tzacc"){
			float xacc = 0;
			if (ist.peek() == 'x')
				xacc = readfloat( "xacc" );
			float zacc = readfloat( "zacc" );
			vzacc.push_back( make_pair( t, lpzacc( zacc - 2.4f )*0.1f ) );
			rs->add_acc( t, xacc, zacc );
		} else if (timestring == "tard"){
			if ( balance_to_add != 0 ){
				rs->add_bal( t, balance_to_add );
				balance_to_add = 0;
			}
			auto steps1 = readint( "steps1" );
			auto steps2 = readint( "steps2" );
			auto speed = readfloat( "speed" );
			auto pwm = readint( "pwm" );
			auto volt = readfloat( "volt" );
			auto mode = readint( "mode" );
			laststeps = 0.5f*(steps1 + steps2);
			rs->add_ard( t, laststeps, speed, pwm, volt, mode );
		} else if (timestring == "balance" ){
			if (lastbalance){
				float dsteps = laststeps - steplog;
				if (dsteps * lastbalance > 0)
					vbal.push_back( make_pair( lastbalance-sign(lastbalance) * 13 , dsteps ) );
			}
			steplog = laststeps;
			lastbalance = t;
			balance_to_add = t;
		}
	}
	PyPlotter pz( fname + "_gy.py" );
	pz.label("zacc").plot2( vzacc );
	pz.label("gy").plot2( vgy );
	if (vbal.size() > 10){
		PyPlotter plt( "bal.py" );
		auto pp = linear_regression( vbal );
		plt.plot2( VPDD{ { -300, -pp.first * 300 + pp.second }, { 300, pp.first * 300 + pp.second } } );
		plt.display_ = ".";
		plt.plot2( vbal );
	}
//	sort( rs->v_.begin(), rs->v_.end(), []( const RunEvent* a, const RunEvent* b ){ return a->t < b->t;  } );
	return rs;
}

vector<pair<double, double>> RunLog::find_all_zacc() const
{
	vector<pair<double, double>> res;
	for( auto x : v_ ){
		if( const RunEventAcc* rz = dynamic_cast<const RunEventAcc*>(x) ){
			res.push_back( make_pair( rz->t, rz->zacc ) );
		}
	}
	return res;
}

vector<pair<double, double>> RunLog::find_all_gy() const
{
	vector<pair<double, double>> res;
	for( auto x : v_ ){
		if( const RunEventGy* rz = dynamic_cast<const RunEventGy*>(x) ){
			res.push_back( make_pair( rz->t, rz->gy ) );
		}
	}
	return res;
}

