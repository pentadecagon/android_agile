#include <xutil/fifo.hh>
#include <algorithm>
#include <fstream>
#include <thread>
#include <xutil/rassert.hh>
#include <infodir/xlog.hh>
#include <iomanip>
#include "roller1.hh"
using namespace std;

typedef pair<const char*, double> FPair;
typedef Fifo<FPair, 1024> FifoType;

static FifoType* g_fifo = 0;
static std::mutex g_mutex;
static ofstream ost;
static void writeloop( const string& filename )
{
	std::unique_lock<std::mutex> lck( g_mutex );
//	ofstream ost(filename);
	ost.open(filename);
	ost << fixed << setprecision(4);
	rassert( ost.good() );
	while(1){
		const FPair a = g_fifo->pop();
		if( a.first == 0 )
			break;
		if( a.first[0] == 't' )
			ost << endl;
		ost << a.first << '=';
		if( a.second == int(a.second))
			ost << int(a.second) << ' ';
		else
			ost << a.second << ' ';
		rassert( ost.good() );
	}
	ost << "\nthe end" << endl;
	rassert( ost.good() );
	ost.close();
	rassert( g_fifo->finish_flag );
	delete g_fifo;
	g_fifo = 0;
	log_thread_cpu_time( "fifo writer" );
}

void fifo_writer_start( const string& filename )
{
	rassert( g_fifo == 0 );
	g_fifo = new FifoType();
	std::thread t1( writeloop, filename );
	t1.detach();
}

void fifo_writer_write_double( const char* a, double b )
{

	g_fifo -> push( make_pair( a, b ));
//	ost << a << '=' << b << endl;
}

void fifo_writer_write_int( const char* a, int b )
{

	g_fifo -> push( make_pair( a, b ));
//	ost << a << '=' << b << endl;
}
void fifo_writer_finish()
{
	LOGV("Try fifo finish...");
	g_fifo ->finish();
	std::unique_lock<std::mutex> lck( g_mutex );
	LOGV("FIFO finished 2");
}

