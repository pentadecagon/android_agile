#pragma once

#include <vector>
#include <iostream>
#include <fstream>
#include <xutil/rassert.hh>
#include <tensor/tinytensor.hh>
#include <infodir/xlog.hh>
#include <xutil/statistics.hh>
#include <xutil/interpolator.hh>
#include <xutil/passfilter.hh>
#include "runparams.hh"
using namespace std;

typedef double ttype;
typedef vector<pair<double, double>> VPDD;


class RunLog;
struct RunState;
struct RunResults;

class RunEvent {
public:
	const ttype t;
	RunEvent( ttype t0 ) : t(t0){}
	virtual ~RunEvent(){}
	virtual void apply( RunState& rs, RunResults* rr ) const = 0;
};

class RunEventGy : public RunEvent {
public:
	const float gy;
	RunEventGy( ttype t, float a ) : RunEvent( t ), gy( a ){}
	void apply( RunState& rs, RunResults* rr ) const;
};

class RunEventAcc;
void apply_acc( RunState& rs, const RunEventAcc& acc, RunResults* rr );

class RunEventAcc : public RunEvent {
public:
	const float xacc;
	const float zacc;
	RunEventAcc( ttype t, float a, float b ) : RunEvent( t ), xacc( a ), zacc(b){}
	virtual void apply( RunState& rs, RunResults* rr ) const { apply_acc( rs, *this, rr ); }
};

class RunEventBal : public RunEvent {
public:
	const float bal;
	RunEventBal( ttype t, float a ) : RunEvent( t ), bal( a ){}
	void apply( RunState& rs, RunResults* rr ) const;
};

void apply_ard( RunState& rs, double t, float steps, float speed, int pwm, float volt, int mode, RunResults* rr );

class RunEventArd : public RunEvent {
public:
	RunEventArd( double ts, float steps0, float speed0, int pwm0, float volt0, int mode0 ) :
		RunEvent( ts ), steps( steps0 ), speed( speed0 ), pwm( pwm0 ), volt( volt0 ), mode( mode0 )
	{}
	virtual void apply( RunState& rs, RunResults* rr ) const {
		apply_ard( rs, t, steps, speed, pwm, volt, mode, rr );
	}

	const float steps;
	const float speed;
	const int pwm;
	const float volt;
	const int mode;
};


enum RunLevel { LevelZero,
	LevelStatic,
	LevelFlying
};

struct RunState {
	RunState( const RunParams& param0 );
	void apply_gy( double ts, float gy, RunResults* rr );
	int next_start_speed() const;
	const RunParams param;

	RunLevel level = LevelZero; // constructor;
	float zacc = 0;

	DVec4 xang;  // by start_accel
	DMat44 Pang; // by start_accel, start flying
	double noise = -DBL_MAX; // start_accel
	double tgy = -DBL_MAX; // apply gy
	double cost = 0.0;
	double sq = 0.0;
	int nsq = 0;
	double stepacc = 0;
	double stepspeed = 0;
	int pwm = 0;
	double prev_balance_t = 0;
	Interpolator step_interpol;
	LowPass<double> lpgy;
	LowPass<double> lpzacc;
	LowPass<double> lpspeed;
	LowPass<double> lpdelta;
	double dgy1 = 0;
	double ddgy1 = 0;
	double dddgy1 = 0;

	int balance_col = 0;
	double balance_t = 0;
	double balance_val = 0;
	double balance_bal = 0;
	Regression zacc_reg;
	double outer_acc = 0;
};


struct RunResults {
	vector<pair<double, double>> vtpsi;
	vector<pair<double, double>> vtouter;
	vector<pair<double, double>> vtpsidot;
	vector<pair<double, double>> vtpsitotal;
	vector<pair<double, double> > vdelta;
	vector<pair<double, double> > vdelta0;
	vector<pair<double, double> > vzacc;
	vector<pair<double, double> > vstepspeed;
	vector<pair<double, double> > vbalance[2];
};

class RunLog {
public:
	RunLog( time_t start_time_, const string& fname0 );
	const RunEventGy* add_gy( double t, float a );
	const RunEventBal* add_bal( double t, float a );
	const RunEventAcc* add_acc( double t, float a, float b );
	const RunEventArd* add_ard(  double t, float steps, float speed, int pwm, float volt, int mode );

	vector<pair<double, double>> find_all_zacc() const;
	vector<pair<double, double>> find_all_gy() const;
	template<class T>
	vector<pair<double, double>> find_all_ard(const T& func) const {
		vector<pair<double, double>> res;
		for (auto x : v_){
			if (const RunEventArd* rz = dynamic_cast<const RunEventArd*>(x)){
				res.push_back(make_pair(rz->t, func(rz)));
			}
		}
		return res;
	}
	vector<pair<double, double>> find_all_steps() const {
		return find_all_ard( [](const RunEventArd* ard){ return ard->steps; } );
	}
	vector<pair<double, double>> find_all_speed( ) const {
		return find_all_ard( []( const RunEventArd* ard ){ return ard->speed; } );
	}
	vector<pair<double, double>> find_all_pwm( ) const {
		return find_all_ard([](const RunEventArd* ard){ return ard->pwm; });
	}
	void run(RunState& rs, RunResults*) const;

	// data
	const string fname_;
	int startspeed_;
	const time_t start_time_;
	vector<RunEvent*> v_;
private:
	~RunLog();
};

const RunLog* load_runlog( const string& fname );
void evaluate_startspeed( RunState& rs );
