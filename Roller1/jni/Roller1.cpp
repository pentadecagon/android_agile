#include <jni.h>
#include <nlopt.h>
#include <assert.h>
#include <string>
#include <infodir/xlog.hh>
#include <iostream>
#include <android/log.h>
#include <xutil/rassert.hh>
#include "roller1.hh"
#include <android/sensor.h>
#include <unistd.h>
#include <sys/resource.h>
#include <xutil/string2type.hh>
#include <thread>

template<class T>
double nlopt_lambda_aux( unsigned int n, const double*x, double* grad, void* v )
{
	const T& lamb = *( T*) v;
	return lamb( n, x, grad );
}

template<class T>
nlopt_opt nlopt_create_lambda(nlopt_algorithm algorithm, unsigned n, const T& t )
{
		nlopt_opt opt = nlopt_create( algorithm, n );
	    nlopt_set_min_objective(opt, &nlopt_lambda_aux<T>, (void*)&t );
		return opt;
}

double sqr(double x){ return x*x; }

float nltest()
{
	auto costfun = [&]( int n, const double* x, double* grad ){
		return sqr( x[0]-3.1) + sqr( x[1]-4.2 ) + 0.1;
	};

	nlopt_opt opt = nlopt_create_lambda( NLOPT_LN_BOBYQA, 2, costfun );
	nlopt_set_xtol_rel( opt, 1e-3);
	double minf = 1e27;

	double vx[2] = { -1, -1 };
	const nlopt_result ro = nlopt_optimize( opt, vx, &minf );
	assert( ro>0 );
	LOGI("BBBBBBBBBBBBB");
    return minf;
}

void fifo_test()
{
	fifo_writer_start("/sdcard/Roller/fifo.txt");
	for( double k=2; k<=122; ++k ){
		fifo_writer_write_double( "ttest", k );
		usleep(200000);
	}
	fifo_writer_finish();
}

extern "C"
jfloat Java_tk_roller1_Native_nativeTest(JNIEnv* env, jclass clazz )
{
	LOGI( " # TADA # " << 17.2);
	return 0.0;
	clock_t t0 = clock();
	int n = 0;
	while( clock() < t0 + CLOCKS_PER_SEC )
		++n;
	LOGI(" clock test: " << n );
	return nltest();
}

//static RunLog* current_run;
static RunState* g_runstate = 0;
double g_start_time = -1;

void process_one_event( const ASensorEvent& ev )
{
	double t = ev.timestamp * 1e-9 - g_start_time;
	if( ev.type == ASENSOR_TYPE_ACCELEROMETER ){
		static double last_t = 0;
		if( t - last_t > 0.009 )
			LOGE("ttt ac " << last_t << " " << t );
		last_t = t;
//		float val = ev.vector.z;
		fifo_writer_write_double( "tacc", t );
		fifo_writer_write_double( "yacc", ev.vector.y );
		fifo_writer_write_double( "zacc", ev.vector.z );
//		g_runstate->apply_zacc( t, val, 0 );
	} else {
		rassert( ev.type == ASENSOR_TYPE_GYROSCOPE);
		static double last_t = 0;
		if( t - last_t > 0.009 )
			LOGE("ttt gy " << last_t << " " << t );
		last_t = t;
		float val = ev.vector.y;
		fifo_writer_write_double( "tgy", t );
		fifo_writer_write_double( "gy", val );
//		g_runstate->apply_gy( t, val, 0 );
	}
}

#if 0
extern "C"
void Java_tk_roller1_Native_addGy(JNIEnv* env, jclass clazz, jlong t0, jfloat val )
{
	if( g_start_time != -1 ){
		double t = t0 * 1e-9 - g_start_time;
		g_runstate->apply_gy( t, val, 0 );
		fifo_writer_write( "tgy", t );
		fifo_writer_write( "gy", val );
	}
}

extern "C"
void Java_tk_roller1_Native_addAcc(JNIEnv* env, jclass clazz, jlong t0, jfloat val )
{
	if( g_start_time != -1 ){
		double t = t0 * 1e-9 - g_start_time;
		g_runstate->apply_zacc( t, val, 0 );
		fifo_writer_write( "tzacc", t );
		fifo_writer_write( "zacc", val );
	} else if( g_runstate->level == LevelZero ){
		g_runstate->level = LevelResting;
		g_runstate->zacc = val;
	}
}
#endif

double current_time()
{
	timeval tv;
	gettimeofday( &tv, 0 );
	return tv.tv_sec + tv.tv_usec*1e-6;
}

struct OutMsg {
	int16_t voltage;
	uint16_t speed;
	int16_t step1;
	int16_t step2;
	uint8_t check_byte;
	uint8_t cur_dir;
	uint8_t error_code;
	uint8_t pwm;
	uint8_t mode;
	uint8_t x14; // reserved, to round the size to 16
	uint8_t x15; // reserved, to round the size to 16
	uint8_t x16; // reserved, to round the size to 16
};

void decode_and_apply(JNIEnv* env,  double t, jbyteArray fromArd )
{
	rassert( env->GetArrayLength(fromArd) == 16 );
	 jbyte* a = env->GetByteArrayElements( fromArd, 0 );
	rassert( (1 & (size_t)a) == 0 );
	const OutMsg* msg = (OutMsg*) a;

	const float volt = msg->voltage * 780.0f / 65536.0f;
	const int ard_pwm = (int)(msg->pwm) * (msg->mode == 0 ? 1 : -1 );

	// speed in steps per second
	const int ardspeed = (msg->speed == 0) ? 0
			: (msg->cur_dir==0 ? 1 : -1)*1000000.0f /  msg->speed;

	env->ReleaseByteArrayElements( fromArd, a, 0 );

	apply_ard( *g_runstate, t, msg->step1, ardspeed, ard_pwm, volt, msg->mode, 0 );
	fifo_writer_write_double( "tard", t );
	fifo_writer_write_int( "steps1", msg->step1 );
	fifo_writer_write_int( "steps2", msg->step2 );
	fifo_writer_write_double( "speed", ardspeed );
	fifo_writer_write_int( "pwm", ard_pwm );
	fifo_writer_write_double( "volt", volt );
	fifo_writer_write_int( "mode", msg->mode );
}



void encode_ard3(JNIEnv* env, jbyteArray toArd, int a, int b, int c )
{
	rassert( env->GetArrayLength(toArd) == 4 );
	jbyte* p = env->GetByteArrayElements( toArd, 0 );

	p[0] = a;
	p[1] = b;
	p[2] = c;

	env->ReleaseByteArrayElements( toArd, p, 0 );
}

void encode_ard2(JNIEnv* env, jbyteArray toArd, int a, int bc )
{
	if( bc < 0 ) bc += 65536;
	encode_ard3( env, toArd, a, bc % 256, bc/256 );
}

void sigfpe(int)
{
	LOGE("FPE");
}

void priority()
{
	int pid = getpid();
	int prio = getpriority( PRIO_PROCESS, 0 );
	LOGI( "Prio=" << prio );
	string s = string("su -c renice 4294967286 ") + type2string(pid);
//	system(s.c_str() );
	prio = getpriority( PRIO_PROCESS, 0 );
	LOGI( "Prio=" << prio );
}

extern "C"
jboolean Java_tk_roller1_Native_startNewRun(JNIEnv* env, jclass clazz, jstring fname )
{
	sleep(2);
	g_runstate_mutex.lock();
	signal( SIGFPE, sigfpe );
	rassert( g_start_time == -1 && g_runstate == 0 );
//	set_prof();
	g_start_time = current_time();
	g_runstate = new RunState( RunParams::default_params() );
	priority();
	const char* outFile = env->GetStringUTFChars( fname, 0 );
	fifo_writer_start( outFile );
	LOGI("Started new Run: " << outFile << " t=" << g_start_time );
	env->ReleaseStringUTFChars( fname, outFile );

	g_runstate_mutex.unlock();
	sensors_init();
	return true;
}

// must not be locked
void shutdown_run()
{
	sensors_destroy();
	std::lock_guard<std::mutex> lck (g_runstate_mutex);
//	fifo_writer_write("phi_end", g_runstate->phi_end);
	fifo_writer_finish();
	g_start_time = -1;
//	evaluate_startspeed(*g_runstate);
	delete g_runstate;
	g_runstate = 0;
	log_thread_cpu_time( "main" );
}

extern "C"
void Java_tk_roller1_Native_stopRun(JNIEnv* env, jclass clazz )
{
	shutdown_run();
}
#if 0
extern "C"
jint Java_tk_roller1_Native_calcBalance(JNIEnv* env, jclass clazz )
{
	std::lock_guard<std::mutex> lck (g_runstate_mutex);
	int dt = what_now( *g_runstate );
	if( dt != 0 ){
		fifo_writer_write("\nbalance",  dt);
		LOGI("Balance: " << dt );
	}
	return dt;;
}
#endif

extern "C"
jboolean Java_tk_roller1_Native_next(JNIEnv* env, jclass clazz,
		jbyteArray toArd, jbyteArray fromArd, jlong tmillis  )
{
	rassert( g_start_time > 0 );
	signal( SIGFPE, sigfpe );

	g_runstate_mutex.lock();

	sensors_process_tmax(tmillis * 1000000ULL);
	const double t = tmillis * 1e-3 - g_start_time;
	decode_and_apply( env, t, fromArd );
	sensors_process_all();
	int dt = what_now( *g_runstate );

	g_runstate_mutex.unlock();
	if( dt==1<<24 ){
		shutdown_run();
		return false;
	} else if( dt != 0 ) {
		fifo_writer_write_double("balance", dt );
		fifo_writer_write_double("psi", g_runstate->xang[0] );
		fifo_writer_write_double("psidot", g_runstate->xang[1] );
		encode_ard2( env, toArd, 18, dt );
		return true;
	} else {
		usleep(3000);
		encode_ard3( env, toArd, 0, 0, 0 );
		return true;
	}
}
#if 0
extern "C"
jint Java_tk_roller1_Native_addArd(JNIEnv* env, jclass clazz,
		jlong t0millis, jint steps1, jint steps2, jfloat speed, jint pwm, jfloat volt, jint mode )
{
	if( g_start_time == -1 )
		return 0;
	rassert( g_start_time != -1 );
	std::lock_guard<std::mutex> lck (g_runstate_mutex);
	sensors_process_tmax(t0millis * 1000000ULL);
	const double t = t0millis * 1e-3 - g_start_time;
	apply_ard( *g_runstate, t, steps1, speed, pwm, volt, mode, 0 );
	fifo_writer_write( "tard", t );
	fifo_writer_write( "steps1", steps1 );
	fifo_writer_write( "steps2", steps2 );
	fifo_writer_write( "speed", speed );
	fifo_writer_write( "pwm", pwm );
	fifo_writer_write( "volt", volt );
	fifo_writer_write( "mode", mode );
	sensors_process_all();
	return 0;//what_now( *g_runstate );
}
#endif
