#include <jni.h>
#include <android/sensor.h>
#include <unistd.h>
#include <time.h>
#include "roller1.hh"
#include <thread>
#include <atomic>
#include <stdint.h>
using namespace std;

mutex g_runstate_mutex;
static ASensorEventQueue * sensorEventQueue1;
static const unsigned int g_fifo_size=32;
static const unsigned int g_fifo_mask = g_fifo_size-1;
static ASensorEvent g_fifo[g_fifo_size];
static const unsigned int g_fifo_rest=4;
static uint_fast16_t g_fifo_wpos;
static uint_fast16_t g_fifo_rpos;
static thread* g_sensor_thread = 0;
static const bool use_callback = true;

static void populate_fifo()
{
	if( use_callback )
		return;
	int total = 0;
	int ktotal = 0;
	while(1){
		const unsigned int wpos1 = (g_fifo_wpos & g_fifo_mask );
		const unsigned int nmax =  g_fifo_size - wpos1;
		const int n = ASensorEventQueue_getEvents(sensorEventQueue1,
				g_fifo+wpos1, nmax);
		if( n==0 )
			break;
		g_fifo_wpos += n;
		total += n;
		ktotal += 1;
	}
	if( total > 6 || total == 0 )
		LOGI("populate: " << total << ' ' << ktotal );
}

int sensor_callback_gy( int a, int b, void* c )
{
	if( ! use_callback )
		rassert( 0 == "invalid callback" );

    const unsigned int wpos1 = (g_fifo_wpos & g_fifo_mask );
    const unsigned int nmax =  g_fifo_size - wpos1;
    const int n = ASensorEventQueue_getEvents(sensorEventQueue1,
            g_fifo+wpos1, nmax);
    if( n >= 4 )
    	LOGI("callback: " << nmax << " " << n );
	g_fifo_wpos += n;
	return 1;
}

// must be locked
void sensors_process_with_rest()
{
	populate_fifo();
//	 if( g_fifo_wpos - g_fifo_rpos  > g_fifo_rest )
//		 LOGV("Rest: " << g_fifo_wpos << ' ' << g_fifo_rpos );
	while ( g_fifo_wpos - g_fifo_rpos  > g_fifo_rest ){
		process_one_event( g_fifo[g_fifo_rpos & g_fifo_mask] );
		++g_fifo_rpos;
	}
}

// must be locked
void sensors_process_all()
{
	while(1){
		populate_fifo();
		if( g_fifo_rpos == g_fifo_wpos )
			return;
//		LOGV("All: " << g_fifo_wpos << ' ' << g_fifo_rpos );
		while ( g_fifo_rpos != g_fifo_wpos ){
			process_one_event( g_fifo[g_fifo_rpos & g_fifo_mask] );
			++g_fifo_rpos;
		}
	}
}

// must be locked
void sensors_process_tmax( uint64_t tmax_nanos )
{
	while(1){
		populate_fifo();
		if( g_fifo_rpos == g_fifo_wpos )
			return;
//		LOGV("Process tmax: " << g_fifo_wpos << ' ' << g_fifo_rpos );
		while ( g_fifo_rpos != g_fifo_wpos ){
			const ASensorEvent& ev = g_fifo[g_fifo_rpos & g_fifo_mask];
			if( ev.timestamp >= tmax_nanos )
				return;
			process_one_event( ev );
			++g_fifo_rpos;
		}
	}
}

// must be locked
void sensor_thread()
{
	LOGI("init sensors");
	ALooper* looper = ALooper_forThread();
	if(looper == NULL){
//		looper = ALooper_prepare(ALOOPER_PREPARE_ALLOW_NON_CALLBACKS);
		looper = ALooper_prepare(0);
		LOGI("Looper created");
	}
	rassert( sensorEventQueue1 == 0 );
	g_fifo_rpos = g_fifo_wpos = 0;
	ASensorManager* sensorManager = ASensorManager_getInstance();
	const ASensor* accelerometerSensor = ASensorManager_getDefaultSensor(sensorManager, ASENSOR_TYPE_ACCELEROMETER);
	rassert( accelerometerSensor != 0 );
	const ASensor* gySensor = ASensorManager_getDefaultSensor(sensorManager, ASENSOR_TYPE_GYROSCOPE);
	rassert( gySensor != 0 );
	sensorEventQueue1 =   ASensorManager_createEventQueue(sensorManager, looper,  3/*LOOPER_ID_USER*/,
			&sensor_callback_gy,
			0);
	rassert( sensorEventQueue1 != 0 );
	int err;
	err = ASensorEventQueue_enableSensor( sensorEventQueue1, gySensor );
	rassert( err >= 0 );
	err = ASensorEventQueue_setEventRate(sensorEventQueue1, gySensor, 5000);
	rassert( err >= 0 );
	err = ASensorEventQueue_enableSensor( sensorEventQueue1, accelerometerSensor );
	rassert( err >= 0 );
	err = ASensorEventQueue_setEventRate(sensorEventQueue1, accelerometerSensor, 5000);
	rassert( err >= 0 );

	while(1){
		if( use_callback )
			ALooper_pollOnce(1000, 0, 0, 0);
		else
			usleep(10000);

		std::lock_guard<std::mutex> lck (g_runstate_mutex);
		if( sensorEventQueue1 == 0 )
			break;
		sensors_process_with_rest();
	}
	log_thread_cpu_time( "sensor" );
}

void sensors_init()
{
	rassert( g_sensor_thread == 0 );
	g_sensor_thread = new thread( sensor_thread );
}

// must not be locked
void sensors_destroy()
{
	g_runstate_mutex.lock();

	rassert( sensorEventQueue1 != 0 );
	ASensorManager* sensorManager = ASensorManager_getInstance();
	ASensorManager_destroyEventQueue( sensorManager, sensorEventQueue1 );
	sensorEventQueue1 = 0;

	g_runstate_mutex.unlock();

	g_sensor_thread -> join();
	LOGV("sensor thread joined");
	delete g_sensor_thread;
	g_sensor_thread = 0;
}

double thread_cpu_time()
{
	timespec ts;
	clock_gettime( CLOCK_THREAD_CPUTIME_ID, &ts );
	return ts.tv_sec + 1e-9*ts.tv_nsec;
}

void log_thread_cpu_time( const char* name )
{
	LOGI("thread " << gettid() << " " <<  name << " time=" << thread_cpu_time() );
}

