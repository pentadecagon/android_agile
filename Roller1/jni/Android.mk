LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := xutil
LOCAL_SRC_FILES := d:/android/git/main/xutil/obj/local/armeabi-v7a/libxutil.a
include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE    := Roller1
LOCAL_SRC_FILES := Roller1.cpp runstate.cc runlog.cc sensors.cc fifo_writer.cc
LOCAL_LDLIBS := -lnlopt -L$(LOCAL_PATH) -Ld:/android/git/main/xutil/obj/local/armeabi-v7a -llog -landroid
LOCAL_C_INCLUDES=d:/code/nlopt-2.4/api d:/android/git/main/xutil
LOCAL_STATIC_LIBRARIES := xutil
include $(BUILD_SHARED_LIBRARY)
