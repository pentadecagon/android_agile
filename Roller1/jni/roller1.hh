#include <mutex>
#include "runstate.hh"
#include <android/sensor.h>
void fifo_writer_start( const string& filename );
void fifo_writer_write_double( const char* a, double b );
void fifo_writer_write_int( const char* a, int b );
void fifo_writer_finish();
void sensors_process_all();
void sensors_process_tmax( uint64_t tmax_nanos );
void sensors_init();
void sensors_destroy();
double current_time();
void process_one_event( const ASensorEvent& ev );
void log_thread_cpu_time( const char* name );
extern std::mutex g_runstate_mutex;
extern double g_start_time;
