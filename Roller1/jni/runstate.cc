#include <xutil/platform.hh>
#include "runstate.hh"
#include <fstream>
#include <iomanip>
#include <infodir/xlog.hh>
#include <xutil/statistics.hh>
#include <xutil/kalman.hh>
#include <deque>
#include <xutil/passfilter.hh>

#undef PROFILE_HERE
#define PROFILE_HERE

EPropD forward_start_regression;
EPropD backward_start_regression;

RunState::RunState( const RunParams& param0 )
: param( param0 ),
step_interpol(param0.dt_step),
lpgy( param0.filter_gy ),
lpzacc( param0.filter_zacc ),
lpspeed( param0.filter_speed ),
lpdelta( 0.1 ),
zacc_reg(2)
{
	level = LevelFlying;
	noise = param.init_noise;
	Pang( 0, 0 ) = Pang( 1, 1 ) = 4; // large initial uncertainty
}

void RunEventGy::apply( RunState& rs, RunResults* rr ) const
{
	rs.apply_gy( t, gy, rr );
}

void RunEventBal::apply( RunState& rs, RunResults* rr ) const
{
	const double thisval = rs.xang[0] * rs.param.alpha + rs.xang[1] - rs.xang[3];
	if (rr && rs.balance_t){
		const double dval = thisval * exp( -(t - rs.balance_t) * rs.param.alpha ) - rs.balance_val;
		rr->vbalance[rs.balance_col].push_back( make_pair( rs.balance_bal, dval ) );
	}
	rs.balance_col = (rs.balance_bal * bal > 0 ? 1 : 0 );
	rs.balance_t = t;
	rs.balance_val = thisval;
	rs.balance_bal = bal;
}

int next_start_speed_prop( const EPropD& e )
{
	rassert( e.n >= 2 );
	const double a = ( e.n * e.sxy - e.sx*e.sy ) / ( e.n*e.sxx - sqr(e.sx) );
	const double b = (e.sy - a * e.sx) / e.n;
	const double x0 = -b/a;
	if( x0 < 1000 || x0 > 1700 ){
		LOGE_("Bad Startspeed: " << x0 );
		return 1300;
	}
	return lround(x0);
}

/*
void evaluate_startspeed( RunState& rs )
{
	double t1 = rs.t_takeoff;
	double t2 = rs.t_touchdown;
	if( t1<=0 || t2 <= 0 || t2 <= t1 ){
		LOGE("Bad startspeed: " << t1 << " " << t2 << " level=" << rs.level );
		return;
	}
	double dt = t2 - t1;
	if( rs.phi_start * rs.xang[in_phi] > 0 ){
		dt = -dt;
	}
	if( rs.startspeed > 0 ){
		LOGI("forward: before: " << next_start_speed_prop(forward_start_regression) );
		LOGI("Adding: " << rs.startspeed << ' ' << dt );
		forward_start_regression.add( rs.startspeed, 1.0/dt );
		LOGI("forward: after: " << next_start_speed_prop(forward_start_regression) );
	} else {
		LOGI("back: before: " << next_start_speed_prop(backward_start_regression) );
		LOGI("Adding: " << rs.startspeed << ' ' << dt );
		backward_start_regression.add( -rs.startspeed, 1.0/dt );
		LOGI("back: after: " << next_start_speed_prop(backward_start_regression) );
	}
//	level = runlog->end_level_;
}
*/

void apply_acc( RunState& rs, const RunEventAcc& acc, RunResults* rr )
{
	const float vzacc = rs.lpzacc( acc.zacc );
	rs.zacc = acc.zacc;
	if ( abs(rs.stepspeed) < 1 && abs( vzacc - 2.33 ) < 1.0 && abs( rs.xang[0] ) < 0.1 && abs( rs.xang[3] ) < 10){
		if (rr)
			rr->vzacc.push_back( make_pair( rs.xang[0], vzacc ) );
	}
}

#if 0
void apply_steps( RunState& rs, float steps, double t, float speed )
{
	PROFILE_HERE;
	return;
	const double  stepspeed = rs.speed;
//	const double stepspeed2 =  (steps - rs.steps) / (t - rs.tard);
	rs.xang[1] += rs.param.motork * (stepspeed - rs.stepspeed);
	const double steps_expected = rs.stepspeed * (t - rs.tard) + rs.steps;
//	rs.xang[in_phi] += rs.param.motork * (steps - steps_expected);
//	rs.xang[2] += rs.param.motork * (stepspeed - rs.stepspeed) * (t - rs.tard) * 0.2;
}
#endif

void apply_ard( RunState& rs, double t, float steps, float speed, int pwm, float volt, int mode, RunResults* rr )
{
	rs.step_interpol.add( t, steps );
	double a0 = rs.step_interpol.curr_val;
	rs.pwm = pwm;
	while (rs.step_interpol.next() ){
		double a1 = rs.step_interpol.curr_val;
		const double thisspeed = (a1 - a0) / rs.param.dt_step;
		const double nextstepspeed = rs.lpspeed( thisspeed );
		rs.stepacc = nextstepspeed - rs.stepspeed;
		rs.stepspeed = nextstepspeed;
		a0 = a1;
		if (rr)
			rr->vstepspeed.push_back( make_pair( rs.step_interpol.curr_t, rs.stepspeed/200 ) );
	}
}
#if 0
void apply_ard1( RunState& rs, double t, float steps, float speed, int pwm, float volt, int mode, RunResults* rr )
{
	PROFILE_HERE;
	if (0 && steps == rs.steps){
		speed = 0;
	}
	if (rs.level == LevelResting &&  pwm != 0){ // start accelerating
		LOGI_( "Level: Accel" );
		start_accel( rs, t );
	} else if (rs.level == LevelAccelerating && pwm == 0){ // start flying
		LOGI_( "Level: Fly1" );
		rs.level = LevelFlying;
	} else if (rs.level == LevelFlying){
		apply_steps( rs, steps, t, speed );
	} else if (t > 2 && rs.level == LevelResting ){
		LOGI_( "Level: Fly2" );
		start_accel( rs, t );
		rs.level = LevelFlying;
	}
	rs.stepspeed = (steps - rs.steps) / (t - rs.tard);
	rs.speed = speed;
	rs.tard = t;
	rs.pwm = pwm;
	rs.steps = steps;
	if (rr){
//		rr->vtphi.push_back( make_pair( t, rs.xang[2] ) );
//		rr->vtphidot.push_back( make_pair( t, rs.xang[in_phidot] ) );
		//rr->vtpsi.push_back( make_pair( t, rs.xang[0] ) );
		rr->vtpsidot.push_back( make_pair( t, rs.xang[1] - rs.xang[3] ) );
	}
}
#endif

int what_now( RunState& rs )
{
//	LOGV("what: " << rs.level << ' ' << rs.speed << ' ' << rs.pwm );
	if (rs.level != LevelFlying
			|| abs(rs.stepspeed) > 50
			|| rs.tgy < rs.prev_balance_t + 0.1
			|| rs.pwm != 0  )
		return 0;
	// now calculate deviation
	const float dev = rs.xang[0] * rs.param.alpha + rs.xang[1] - rs.stepspeed / 2000;
	const float dev2 = dev / sqrt( rs.noise * 0.005 );
	if (abs( dev )> 0.5 ){
		return 1 << 24;
	} else if ( abs( dev ) < 0.1  || abs(dev2) < 3 )
	{
		LOGI("Deviation=" << dev << ' ' << dev2 );
		return 0;
	} else {
		const double bias = sign( dev ) * rs.param.accel_bias;
		 int res = -lround( dev * rs.param.accel  + bias );
		 LOGI("recommend new speed: " << dev << ' '  << res << ' ' << rs.tgy );
		 rs.prev_balance_t = rs.tgy;
		 return res;
	}
}

DMat44 make_F( double alphaq, double dt, const RunParams& param )
{
	PROFILE_HERE;
	const double F00 = sqrt(1.0 + alphaq*dt*dt);
	const double aq = alphaq;
	const double tq = dt*dt;
	DMat44 F;
	F( 0, 0 ) = F( 1, 1 ) = F00;
	F( 1, 0 ) = dt * aq;
	F( 0, 1 ) = F( 2, 3 ) = dt;
	F( 3, 3 ) = F( 2, 2 ) = 1.0;
	return F;
}

void RunState::apply_gy( double ts, float gy0, RunResults* rr )
{
	const float gy = lpgy( gy0 );
	const double dt = ts - tgy;
	if( dt > 0.01 ){
		LOGE_("Large tgy difference " << tgy << ' ' << ts );
		tgy = ts;  // FIXME
		return;
	}
	/* Two options here:  Either try to update phi.  Or not. */
	if ( level != LevelFlying ){
		tgy = ts;
		return;
	}
	PROFILE_HERE;
	rassert( tgy > 0 );
	rassert( noise > 0 );
	const double alphaq = sqr( param.alpha );

	noise = noise*param.np1
		+ param.np3*sqr( stepacc )*1e-4
		+ param.np5 * sqr( zacc - param.zacc_midval )*0.01;
	rassert( noise > 0 );
	const double prev_outer = xang[3];
	static DVec4 H{ 0, 1, 0, 0 };
	double nqpsi = noise * dt;
	DMat44 Q;
	Q( 0, 0 ) = Q( 2, 2 ) = Q( 0, 2 ) = Q( 2, 0 ) = nqpsi * dt * dt / 3;
	Q( 1, 0 ) = Q( 0, 1 ) = Q(3,0) = Q( 0, 3 ) = nqpsi * dt / 2.0;
	Q( 1, 2 ) = Q( 2, 1 ) = Q( 3, 2 ) = Q( 2, 3 ) = nqpsi * dt / 2.0;
	Q( 1, 1 ) = Q( 1, 3 ) = Q( 3, 1 ) = Q( 3, 3 ) = nqpsi;
//	Q( 2, 2 ) = Q( 0, 0 ) * 0.001;
//	Q( 1, 1 ) = Q( 3, 3 ) * 0.000001;
	const DMat44 F = make_F(alphaq, dt, param );
//	rassert( sum(sqr(xang)) < 100 );
//	rassert( sum(sqr(Pang)) < 10000 );
	const double dfix1 = param.fgy1 * dgy1 + param.ffgy1 * ddgy1 + param.fffgy1 * xang[3];
//	const double dfix3 = param.fgy3 * dgy3 + param.ffgy3 * ddgy3;
	DVec4 Bu = { dfix1 * dt * 0.5, dfix1, dfix1 * dt * 0.5, dfix1 };
	kalman_predict( xang, Pang, F, Bu, Q );
	if (rr){
		rr->vdelta0.push_back( make_pair( ts, sqrt(Pang(0,0)) ) );
		rr->vtpsi.push_back( make_pair( ts, xang[0] ) );
		rr->vtpsidot.push_back( make_pair( ts, xang[1]-xang[3] ) );
		rr->vdelta.push_back( make_pair( ts, xang[3] ) );
	}
//	rassert( Pang( 1, 1 ) >= 0 );
	const double R = sqr( param.sensor_noise );
//	rassert( sum(sqr(xang)) < 100 );
//	rassert( sum(sqr(Pang)) < 10000 );
	const DVec4 xpred = xang;
	const double S = kalman_update( xang, Pang, H, gy, R );
	DVec4 H2{ 0, 0, 0, 1 };
	kalman_update( xang, Pang, H2, 0, 2*nqpsi );
	//	rassert( sum( sqr( xang ) ) < 100 );
//	rassert( sum( sqr( Pang ) ) < 10000 );
	dddgy1 = ddgy1;
	ddgy1 = dgy1;
//	ddgy3 = dgy3;
	const double ggy = gy - xpred[1];// -xpred[3];
	dgy1 = dfix1 + xang[1] - xpred[1];
//	dgy3 = dfix3 + xang[3] - xpred[3];
//	Pang( 0, 0 ) += sqr( dgy1 * dt ) / 6;
	noise += param.np2*sqr( ggy  );
	rassert( noise > 0 );
	tgy = ts;
	if( 1 || level == LevelFlying ){
		const double thissq = sqr( ggy ) / S;
//		rassert( thissq < 10000 );
		sq += thissq;
		nsq += 1;
		const double thiscost = gauss_cost_val( S*1000, 1000*sqr(ggy) );
//		rassert( thiscost < 10000 );
		if ( level == LevelFlying)
			cost += thiscost;
	}
	if (rr){
		rr->vtpsi.push_back( make_pair( tgy, xang[0] ) );
		rr->vtouter.push_back( make_pair( tgy, xang[2] ) );
		rr->vtpsidot.push_back( make_pair( tgy, xang[1] - xang[3] ) );
		rr->vdelta.push_back( make_pair( tgy, xang[3] ) );
		const double totpsi = xang[0] * param.alpha + xang[1];
		rr->vtpsitotal.push_back( make_pair( tgy, totpsi ) );
	}
	if (abs( stepspeed ) < 10){
		outer_acc = (xang[3] - prev_outer) / dt;
		zacc_reg.add2( zacc, xang[1], xang[0] );
	}
}

void RunLog::run( RunState& rs, RunResults* rr ) const
{
	PROFILE_HERE;
//	rs.new_run( startspeed_ );
//	rassert( t_stop > 1 );//&& t_stop < 4 );
	deque<const RunEventArd*> deq;
	for( auto p : v_ ){
/*		if (auto pard = dynamic_cast<RunEventArd*>(p)){
			deq.push_back( pard );
		} else {
			while (deq.size() > 0 && deq.front()->t-1000 < p->t){
				deq.front()->apply( rs, rr );
				deq.pop_front();
			} */
			p->apply( rs, rr );
	}
}

int RunState::next_start_speed() const 
{
	int speed;
	if( zacc > param.zacc_midval ){
		speed = next_start_speed_prop( forward_start_regression );
	} else {
		speed = -next_start_speed_prop( backward_start_regression );
	}
	return speed;
}
