	package tk.roller1;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


public class Roller1 extends Activity {

	private static final String ACTION_USB_PERMISSION = "tk.roller1.action.USB_PERMISSION";
	private PendingIntent mPermissionIntent;
	Handler mHandler;
	TextView mText;
	boolean mPermissionRequestPending = false;
	
	private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (ACTION_USB_PERMISSION.equals(action)) {
				synchronized (this) {
                    UsbAccessory accessory = gv.getAccessory();
					if (intent.getBooleanExtra(
							UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
						openAccessory(accessory);
					} else {
						Log.d("ROller", "permission denied for accessory " + accessory);
					}
					mPermissionRequestPending = false;
				}
			} else if (UsbManager.ACTION_USB_ACCESSORY_DETACHED.equals(action)) {
/*				UsbAccessory accessory = UsbManager.getAccessory(intent);
				if (accessory != null && accessory.equals(mAccessory)) {
					closeAccessory();
				}*/
			}
		}
	};
		
	void MountWindows(){
		try{
			final String mountcmd = "/system/bin/mount -t cifs -o  ip=192.168.178.44,unc=\\\\eurydike\\public,username=ThomasKunert,password=tomek,file_mode=0666,dir_mode=0777 //eurydike/public /sdcard/win";
			final String[] command = {"su", "-c",  mountcmd};
			Log.i("Roller1", "command="+command[2] );
			Process process = Runtime.getRuntime().exec( command );
			
			
			int retval = process.waitFor();
			Log.i("Roller1", "su returns: " + retval);
			PrintWriter writer = new PrintWriter("/sdcard/win/t55", "UTF-8");
			writer.println("The first line");
			writer.println("The second line");
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
	}
	@Override
	protected void onDestroy () {
		super.onDestroy();
		Log.e("roller", "Roller1: onDestroy.");
//		Native.stopRun();
	}
	
	private void openAccessory(UsbAccessory accessory) {
		gv.pfd = gv.usbManager.openAccessory(accessory);		
		gv.rassert(gv.pfd != null);
		Intent intent = new Intent(this, RollerService.class);
		startService(intent);
		mText.setText("Service started");
	}
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (gv.pfd != null) {
			Log.e("Roller", "already running");
			finish();
			return;
		}

		gv.context = this;
		
		// some USB stuff
		
		if( gv.usbManager == null ){
			gv.usbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
		}
		mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(
				ACTION_USB_PERMISSION), 0);
		IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
		filter.addAction(UsbManager.ACTION_USB_ACCESSORY_DETACHED);
		registerReceiver(mUsbReceiver, filter);
		
		
		setContentView(R.layout.activity_roller1);
		mText = new TextView(this);
//		MountWindows();
		mText.setText("onCreate");
		setContentView(mText);
	}

	@Override
	public void onResume() {
		super.onResume();

		// Intent intent = getIntent();

		UsbAccessory accessory = gv.getAccessory();
		if (gv.usbManager.hasPermission(accessory)) {
			openAccessory(accessory);
		} else {
			synchronized (mUsbReceiver) {
				if (!mPermissionRequestPending) {
					gv.usbManager.requestPermission(accessory, mPermissionIntent);
					mPermissionRequestPending = true;
				}
			}
		}
	}
	
}
