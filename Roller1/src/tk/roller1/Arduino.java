package tk.roller1;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import tk.rollerrun.RunState.ArdState;
import android.os.SystemClock;
import android.util.Log;

public class Arduino   implements Runnable {
	public FileDescriptor mfd;
	public FileInputStream mInputStream;
	public FileOutputStream mOutputStream;

	final byte[] toArd = new byte[4];
	final byte[] fromArd = new byte[1024];
	byte lastId=0;

	private boolean stopflagService = false; // if this is set the entire service must finish
	private boolean stopflagRun = false; // the current run must stop
	private boolean startflag = false; // start a new run
	private boolean errorflag = false;

	Arduino(){
		mfd = gv.pfd.getFileDescriptor();
		mInputStream = new FileInputStream(mfd);
		mOutputStream = new FileOutputStream(mfd);
	}
		
	long justSend() {
		long tmillis = 0;
		if( errorflag ){
			Log.e("xx", "errorFlag="+errorflag);
			return 0;
		}
		lastId += 1;
		toArd[3] = lastId;
		try {
//			Log.v("xx", "writing ...");
			mOutputStream.write(toArd);
//			Log.v("xx", "reading ...");
			tmillis = System.currentTimeMillis();	
			mInputStream.read(fromArd);
//			Log.v("xx", "Done");
		} catch (IOException e) {
			Log.e("roller1", "Arduino read/write failed");
			gv.error(e);
			e.printStackTrace();
			stopflagRun = stopflagService = errorflag = true;
			gv.service.stopSelf();
			return 0;
		}
		final int check_byte = fromArd[8];
		final int error_code = fromArd[10];
		if( check_byte != lastId ){
			gv.xerror("bad id in sendCommand, got " + check_byte + " but expected " + lastId );
		}
		if( error_code != 0 ){
			gv.xerror("got error code from Arduino: "+error_code );
		}
		return tmillis;
	}
	
	private void runrun(){
		final long tstart = clearMotors();
		boolean good = Native.startNewRun( gv.logfilename() );
		while( good && ! stopflagRun){
			final long t = justSend();
			good = Native.next( toArd, fromArd, t );
		}
		clearMotors();
		if( good ){ // the stop has not been initiated by Native
			Log.e("roller", "runrun: Stop.");
			Native.stopRun();
		}
		stopflagRun = false;
	}
	
	private long clearMotors()
	{
		toArd[0] = (byte) 9;
		toArd[1] = (byte) 0;
		toArd[2] = (byte) 0;
		return justSend();
	}
	
	public void run() {
		Log.i("roller", "Thread running");
		while( ! stopflagService ){
			if( startflag ){
				runrun();
				startflag = false;
			} else  {
				clearMotors();
				SystemClock.sleep(100);
			}
		}
		try { mInputStream.close();} catch (IOException e) {}
		try { mOutputStream.close();} catch (IOException e) {}
		try { gv.pfd.close();} catch (IOException e) {}
		gv.pfd = null;
	}			
	
	public void setStart(){
		startflag = true;
	}
	/*
	synchronized void sendCommand( int a, int b, int c )
	{
		lastId += 1;
		outBuf[0] = (byte) a;
		outBuf[1] = (byte) b;
		outBuf[2] = (byte) c;
		outBuf[3] = lastId;
		long ts_millis;
		try {
			mOutputStream.write(outBuf);
		} catch (IOException e) {
			Log.e("roller1", "sendCommand failed");
			gv.error(e);
			gv.service.stopSelf();
			return;
		}
		ts_millis = System.currentTimeMillis();		
		try {
			mInputStream.read(inbuf);
		} catch (IOException e) {
			Log.e("roller1a", "asendCommand failed");
			gv.error(e);
			gv.service.stopSelf();
			return;
		}
		
		final float voltage = (ti(inbuf[0]) + inbuf[1] * 256)*(780.0f/65536.0f);
		final int speed = ti(inbuf[2]) + ti(inbuf[3])*256;
		final short ard_steps1 = (short) (ti(inbuf[4]) + inbuf[5] * 256);	
		final short ard_steps2 = (short) (ti(inbuf[6]) + inbuf[7] * 256);	
		final int check_byte = inbuf[8];
		final int cur_dir = inbuf[9];
		final int error_code = inbuf[10];
		final int mode = inbuf[12];
		final short ard_pwm = (short)( ti(inbuf[11]) * (mode == 0 ? 1 : -1) );
		int ard_speed; // speed in steps per second
		if( speed==0 ) {
			ard_speed = 0;
		} else {
			ard_speed = (int)(  (cur_dir==0 ? 1 : -1)*1000000.0f /  speed);
		}
		
		if( check_byte != lastId )
			gv.xerror("bad id in sendCommand, got " + check_byte + " but expected " + lastId );
		
		if( error_code != 0 ){
			gv.xerror("got error code from Arduino: "+error_code );
		}
		Native.addArd( ts_millis, ard_steps1, ard_steps2, ard_speed, ard_pwm, voltage, mode);
	}
	*/
	public void close(){
		Log.w("roller1", "Arduino close");
		stopflagService = stopflagRun = true;
	}
}
