package tk.roller1;

// Global Variables

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import tk.rollerrun.RunState;
import android.content.Context;
import android.hardware.usb.UsbAccessory;
import android.hardware.usb.UsbManager;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.os.SystemClock;
import android.util.Log;

public class gv {
	public static ParcelFileDescriptor pfd;
	// global single instance classes
//	public static SensorManager sensorManager;
	public static UsbManager usbManager;
	public static RollerService service;
	public static Arduino ard;
	public static HServer hserver;
	public static Sensors sensors;
//	public static Controller controller;
	public static RunState runState;
	public static Context context;
	// written once values
	public final static long appStartTime = SystemClock.uptimeMillis();
	public final static float gyro_bias = 0.02f;
	// text
	// public static StringBuilder data_text;
	public static StringBuilder log_text = new StringBuilder();
	public static float[] data_text_sync = new float[2];

	static float sqr(float x) {
		return x * x;
	}

	/*
	 * static double accel_abs() { return Math.sqrt( sqr(
	 * gravity[0]-accel_val[0] ) + sqr( gravity[1]-accel_val[1] ) + sqr(
	 * gravity[2]-accel_val[2] ) ); }
	 */
	static void cp3(float[] a, float[] b) {
		a[0] = b[0];
		a[1] = b[1];
		a[2] = b[2];
	}

	static final int Mode_Forward = 0;
	static final int Mode_Backward = 1;
	static final int Mode_Stop = 4;

	// final static String data_text_date = new
	// SimpleDateFormat("yyyy-MM-dd_HH.mm.ss_").format(new Date());

	/*
	 * static void initDataText(){ final String data_text_first_line =
	 * data_text_date + (SystemClock.uptimeMillis() - appStartTime)/1000 + '#';
	 * data_text = new StringBuilder(100000); data_text.append(
	 * data_text_first_line ); }
	 * 
	 * static void finishDataText(){ hserver.setData( data_text.toString());
	 * data_text = null; }
	 * 
	 * static void update_phi( int speed_ticks, // from arduino float new_pos,
	 * float betadot ){ // here we apply the calman filter to calculate new
	 * values of phi and phidot
	 * 
	 * }
	 */

	// static int startmillis;

	// static float phi_dot = 0;
//	static long runStartMillis;

	static void xerror(String s) {
		Log.e("roller1", s);
		gv.log_text.append(s);
	}

	static void error(Exception e) {
		xerror("Exception: " + e.toString());
	}

	static String logfilename(){
		File path = Environment.getExternalStorageDirectory ();
		String s = path.getAbsolutePath() + "/Roller/";
		final String fname = s+new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss", Locale.US).format(new Date())+".txt";
		Log.i("Log name", fname);
		try {
			new File(s).mkdirs();
			new FileWriter(fname).close();
		} catch (IOException e) {
			e.printStackTrace();
			Log.wtf("Roller", "Could not Open File");
		}
//		MediaScannerConnection.scanFile(context, new String[] { fname }, null, null);
		return fname;
	}

	static String ChargeFileName="/sys/devices/platform/msm_ssbi.0/pm8921-core/pm8921-charger/charge";
	
	public static int ReadChargeStatus(){
		try {
			BufferedReader br = new BufferedReader(new FileReader(ChargeFileName));
		    int c = br.read()-48;
		    br.close();
		    Log.i("Charger", "charge="+c);
		    return c;
		} catch ( Exception e ){
			e.printStackTrace();
			return -1;
		}
	}
	public static void rassert(boolean b) {
		if (!b)
			throw new RuntimeException("very bad");
	}
	
	public static UsbAccessory getAccessory() {
        UsbAccessory[] accessories = gv.usbManager.getAccessoryList();
        rassert(accessories.length == 1);
        return accessories[0];
	}
}
