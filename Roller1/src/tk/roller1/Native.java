package tk.roller1;

public class Native {
    public static native float nativeTest();
//    public static native synchronized void addGy( long t, float val );
//    public static native synchronized void addAcc( long t, float val );
//    public static native synchronized int addArd( long tmillis, int steps1, int steps2, float speed, int pwm, float volt, int mode );
//    public static native synchronized void newRun( String fname );
//    public static native synchronized void closeRun();

    public static native boolean startNewRun( String fname );
    public static native boolean next(byte[] toArd, byte[] fromArd, long tmillis );
    public static native void stopRun();
    
//    public static native synchronized void initSensors();
//    public static native synchronized int calcStartSpeed();
//    public static native synchronized int calcBalance();
    
    static {
        System.loadLibrary("Roller1");
   }
}
