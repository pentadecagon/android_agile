package tk.roller1;

import java.io.IOException;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

public class RollerService extends Service {
	Thread mThread;
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
//		Sensors.registerAll();
		if( gv.service != null ){
			gv.xerror("Service is already Running!");
			return START_STICKY;
		}
		
		gv.service = this;
		Toast.makeText(this, "Roller-service starting", Toast.LENGTH_SHORT).show();
//		gv.sensors = new Sensors();
		gv.ard = new Arduino();
		
		try {
			gv.hserver = new HServer(getApplicationContext());
		} catch (IOException e) {
			gv.error(e);
		}
		Notification notification = new Notification(R.drawable.rabbit, "Roller",
		        System.currentTimeMillis());		
		Intent notificationIntent = new Intent(this, Roller1.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		notification.setLatestEventInfo(this, "Roller",
		        "", pendingIntent);
		startForeground(4325, notification);
//		gv.controller = new Controller();
		mThread = new Thread( gv.ard );
		mThread.start();
		return START_STICKY;
	}
	
	public synchronized void onDestroy()
	{
//		gv.controller.setTerminate();
		gv.hserver.setData("");
		gv.hserver.stop();
//		gv.sensors.close();
//		gv.controller=null;
		gv.ard.close();
		try {
			mThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		gv.ard=null;
		gv.hserver=null;
//		gv.sensors = null;
//		Sensors.close();
		Log.i("roller1", "Service Destroyed");
		gv.service = null;
	}

	@Override
	public IBinder onBind(Intent intent) {
		gv.xerror("onBind");
		return null;
	}
}
