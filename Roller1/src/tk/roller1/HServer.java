package tk.roller1;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.Properties;

import android.content.Context;
import android.os.SystemClock;

public class HServer extends NanoHTTPD
{
	String htext;
	final String maintext;
	void logger(String s){
		htext += s+"\n";
	}
	private String datatext = null;
	public HServer(Context ctx) throws IOException
	{
		super(8087, new File("."));
		htext="27 started at " + new Date().toString() + "\n###\n";
		InputStream inputStream = ctx.getResources().openRawResource(R.raw.main);
        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader br = new BufferedReader(inputreader);
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append("\n");
                line = br.readLine();
            }
            maintext = sb.toString();
        } finally {
            br.close();
        }
	}
	public synchronized void setData( String s )
	{
		datatext = s;
		notify();
	}
	private synchronized String waitForData() {
		if( datatext == null ) {
			try {
				wait();
			} catch (InterruptedException e) {
				return "";
			}
		}
		String s = datatext;
		datatext=null;
		return s;
	}
	
	public Response serve( String uri, String method, Properties header, Properties parms, Properties files )
	{
		if( uri.equals("/terminate")){
			gv.service.stopSelf();
		} else if( uri.equals("/start")){
			try {
//				gv.start_speed = Integer.parseInt( parms.getProperty("pwm") );
			} catch(NumberFormatException e){
			}
				gv.ard.setStart();
		} else if( uri.equals("/data") ){
			return new NanoHTTPD.Response(HTTP_OK, MIME_PLAINTEXT, waitForData() );
		} else if( uri.equals("/log") ){
			return new NanoHTTPD.Response(HTTP_OK, MIME_PLAINTEXT, gv.log_text.toString() );
		} else if( uri.startsWith("/navigate") ) {
//			gv.controller.setNavigate( Integer.parseInt( parms.getProperty("key") ) );
		}
		String msg = maintext.replace("LOGTEXT",  gv.log_text.toString());
		return new NanoHTTPD.Response( HTTP_OK, MIME_HTML, msg );
	} 
}
