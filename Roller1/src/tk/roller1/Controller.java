package tk.roller1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import tk.rollerrun.RunState;
import tk.rollerrun.RunState.Params;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;

public class Controller  implements Runnable {
	private boolean bStart = false;
	private boolean bTerminate = false;
	private int _navigate = 0;
	void setTerminate(){
		bTerminate = true;
	}
	
	void setStart(){
		bStart = true;
	}
	void setNavigate( int key ){
		_navigate = key;
	}
	private Params defaultParams(){
		Params p = new Params();
		p.alpha = 7.1;
        p.qf = 0.5;
        p.noise_v=0.1;
        p.zacc_noise=0.5;
        p.zacc_zero=1.76;
        p.noise_pos=0.005;
		p.noise_gyro=0.013;
		p.init_phi=-0.18;
		return p;
	}

	void sendAndLog( int a, int b, int c ) {
//		gv.ard.sendCommand(a, b, c);
	}
	void sendAndLog2( int a, int s ){
		if( s<0 )
			s += 65536;
		sendAndLog( a, s%256, s/256 );
	}

	void wait_for_motor(int millis) {
		final long h = SystemClock.uptimeMillis();
		while( SystemClock.uptimeMillis() - h < millis ){
			SystemClock.sleep(10);	
			sendAndLog(0,0,0);
		}
	}

	void doCommand(){
		if( _navigate== 38 ){ // forward
			sendAndLog( 1, 0, 150 );
		} else if( _navigate == 40 ){ //back
			sendAndLog( 1, 1, 150 );
		} else if( _navigate==39 ){ // right
			sendAndLog( 15, 0, 150 );
		} else if( _navigate == 37 ){ //left
			sendAndLog( 14, 0, 150 );
		} else {
			_navigate = 0;
			return;
		}
		_navigate=0;
		SystemClock.sleep(200);
		sendAndLog( 10, 0, 0 );
	}

	void doSimpleRun()
	{
//		gv.runStartMillis =  System.currentTimeMillis();
//		Native.newRun(gv.logfilename()  );
		sendAndLog( 9, 0, 0 );
		wait_for_motor(500);
/*		final int startSpeed = Native.calcStartSpeed();
		int direction = startSpeed > 0 ? 0 : 1;
		Log.i("roller1", "start speed=" + startSpeed + " dir=" + direction );
		sendAndLog(1, direction, 0);
		sendAndLog2( 12, Math.abs(startSpeed) );*/
//			while(true){
//			final int b = Native.calcBalance();
/*			if( b == 1<<24 )
				break;
			if( b != 0 ){
				sendAndLog2( 18, -b );
			} else {
				sendAndLog( 0, 0, 0 );
			}
			SystemClock.sleep(10);
		}*/
//		wait_for_motor(2000);
//		Native.closeRun();
//		SystemClock.sleep(2000);
//		sendAndLog( 9, 0, 0 );
	}
	
	void balance()
	{
//		Native.newRun(gv.logfilename()  );
		sendAndLog( 9, 0, 0 );
		for( int i=0; i<200; ++i ){
			
		}
	}
	
	void doStart()
	{
		bStart = false;
//		gv.runStartMillis =  System.currentTimeMillis();
		for( int k=0; k<1; ++k ){
			doSimpleRun();
		}
	}
		
		
//		gv.runState = new RunState( defaultParams() );
/*		
		while(true){
			for( int i=0; i<5; ++i ){
				SystemClock.sleep(10);	
				sendAndLog(0,0,0);
				if( Math.abs(Arduino.lastArd._speed) >100 || Arduino.lastArd._pwm !=0 ){
					i=0;
				}
			} // speed is 26/ms
			final double dev = gv.runState.getDeviation();
			int newspeed = (int) Math.abs( dev * 100.0 );
			gv.runState.logDouble( "deviation",  dev );
			gv.runState.logDouble( "time", SystemClock.uptimeMillis() - gv.runStartMillis );
//			 Log.i("roller1",  "state: " + gv.runState._x + " dev: " + dev + " speed: " + newspeed );
			if( newspeed>100 ){
				break;
			} else if( newspeed > 3 ){
				int new_dir = dev>0 ? 0 : 1;
				gv.runState.logDouble( "time", SystemClock.uptimeMillis() - gv.runStartMillis );
				sendAndLog( 17, new_dir+254, newspeed );
				gv.runState.logDouble( "time", SystemClock.uptimeMillis() - gv.runStartMillis );
			} else {
				SystemClock.sleep(10);	
			}
		} */
	
	public void run() {
/*		for( int dt=-400; dt<300; dt+=600 ){
			int absdt = dt < 0 ? dt+65536 : dt;
			gv.ard.sendCommand( 18, absdt % 256, absdt / 256);
			SystemClock.sleep(2000);
		} */

//		Native.newRun(gv.logfilename()  );
		while( ! bTerminate ){
			if( bStart ){
				doStart();
			} else if( _navigate != 0 ){
				doCommand();
			} else { 
//				gv.ard.sendCommand(0,0,0);
				SystemClock.sleep(50);
			}
		}
	}
}
