package tk.rollerrun;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.lang.Double;
import tk.XMatrix.XMat22;
import tk.XMatrix.XVec2;

/* We need to Log:
 * parameters
 * sensor values
 * arduino values
 * controller actions
 * evaluated state result
 */

public class RunState {
	Logger _logger;
	final Params _params;

	// state values
	
	public XVec2 _x;
	double _deviation = 0;
	XMat22 _P;
	double gnoise;
	int last_gyro_t; // millis
	ArdState last_ard; // millis
	double _totalCost = 0;
	static double sqr( double x ){
		return x*x;
	}
	
	public enum Mode { RestBack, RestFront, Flying, Unknown };
	
	public Mode mode = Mode.Unknown;
	XVec2 _K;
	public double kalman_step( double dt, double gy ){
		double alpha;
		if( mode == Mode.RestBack || mode == Mode.RestFront || last_ard._speed != 0 || last_ard._pwm != 0 )
			alpha = 0;
		else 
			alpha = _params.alpha;
		final double cosh = 1.0 + sqr( alpha * dt ) * 0.5;
	    final XMat22 F = new XMat22( cosh, dt,
	                    sqr(alpha)*dt, cosh);
	    final double dv = Math.sqrt( gnoise ) *  _params.noise_v;
	    final double ds = /*Math.sqrt( gnoise ) */  _params.noise_pos;
	    final double ds0 = dv*dt*_params.qf;   // the position noise resulting from the v-noise
	    final double dv1 = ds*dt*sqr(alpha)*_params.qf; // the v-noise resulting from the position noise
	    final double Q01 = (ds*dv1 + dv*ds0);
	    final XMat22 Q=new XMat22( (ds*ds+ds0*ds0),    Q01,
	                Q01,                     (dv*dv+dv1*dv1));

	    final XVec2 x1 = F.dot(_x); //+ Bu
	    final XMat22 P1 = F.dot(_P).dot(F.transpose() ).add(Q);
	    
	    final XVec2 H=new XVec2( 0, 1 );
	    final double R=sqr(_params.noise_gyro);
	    	    
	    final double y = gy - x1.a1;
	    final double S = P1.a11 + R;
	    final XVec2 K = P1.dot(H).mult(1.0/S);
	    _K = K;
	    final XVec2 x2 = K.mult(y).add(x1);
	    final XMat22 P2 = K.outer(H).oneless().dot( P1);
	    double cost = 0.5*(Math.log(S) + y*y/S);
//	    cost = y*y;
	    _x = x2;
	    _P = P2;
//	    if( alpha>0 )
	    	_totalCost += cost;
		return cost;	
	}
	
	public static class Params {
		public double alpha = -1;
		public double noise_v = -1;
		public double noise_pos = -1;
		public double noise_gyro = -1;
		public double zacc_noise = -1;  
//		public double pwm_noise = -1;
		public double zacc_zero = -1;  // zacc value in the balanced case
		public double init_phi = -1;
		public double qf = -1;
		public String toString(){
			String erg="";
			for( Field f: this.getClass().getDeclaredFields() ){
				try {
					String value = f.get(this).toString();
					erg += "param_"+f.getName()+"="+value+"\n";
				} catch (Exception e) {}  // we ignore uninitialized fields here
			}
			return erg;
		}
		public void checkInit() {
			try {
				for( Field f: this.getClass().getDeclaredFields() ){
					if( f.get(this).equals( Double.valueOf(-1) ) ){
						throw new Exception("uninitialized value: " + f.getName());
					}
				}
			} catch ( Exception e) {
					e.printStackTrace();
					System.out.println( toString() );
					int a = 1/0;
			}
		}
	}
	public static Params newParams(){ return new Params(); }
//	public static RunState newState(){ return new RunState(); }	

	public RunState( Params p ){
		p.checkInit();
		_params = p;
		// init state values
		_x = new XVec2( p.init_phi, 0 );
		_P = new XMat22( 1e-3, 0, 0, 1e-8 );
		gnoise = 10;
		last_gyro_t = 0;
		_logger = new Logger();
	}
	public void initLog(){
		_logger=new Logger();
	}
	public synchronized String finishLog(){
		return _params.toString() + _logger.finish();
	}
	float _lastZacc = 0;
	void updateZacc( int ts /* milliseconds from run start */, float zacc ){
		_lastZacc = zacc;
		if( mode == Mode.Unknown ){
			if( zacc>0 )
				mode = Mode.RestBack;
			else
				mode = Mode.RestFront;
			gnoise = sqr( zacc-_params.zacc_zero );
		} else {
			gnoise = gnoise * (1-_params.zacc_noise) + _params.zacc_noise * sqr( zacc-_params.zacc_zero );
		}
	}
	
	static public LogZacc newLogZacc( int ts, float zacc ){
		return new LogZacc( ts, zacc );
	}
	static public class LogZacc extends LogElement {
		final int _ts;
		final float _zacc;
		LogZacc( int ts, float zacc ){
			_ts = ts;
			_zacc = zacc;
		}
		public String toString(){
			return "tzacc="+_ts
					+" zacc=" + _zacc
					+ "\n";
		}
		@Override
		public void execute(RunState rs) {
			rs.updateZacc( _ts, _zacc );
		}
	}
	
	public synchronized  void logRunZacc( int ts, float zacc ){
		if( ts<=0 )
			return;
		_logger.add( new LogZacc( ts, zacc ));
		updateZacc( ts, zacc );
	}
	
	double updateGyro( int ts /* milliseconds from run start */, double gy ){
		final int dt = ts - last_gyro_t;
//		xassert( dt>0 );
		last_gyro_t = ts;
		if( ts<=0 || mode == Mode.Unknown )
			return 0;
		double cost = kalman_step( dt*1e-3, gy );
		if( _logger != null ){
			_logger.add( new LogState( 	ts, _x.a0, _x.a1 ) );
		}		
		return cost;
	}
	static public LogGyro newLogGyro( int ts, double gy ){
		return new LogGyro(ts, gy);
	}
	static public class LogGyro extends LogElement {
		final int _ts;
		final double _gy;
		LogGyro( int ts, double gy ){
			_ts = ts;
			_gy = gy;
		}
		public String toString(){
			return "tgy="+_ts
					+" gy="+_gy
					+"\n";
		}
		@Override
		public void execute(RunState rs) {
			rs.updateGyro(_ts, _gy);
		}
	};
	
	static public class LogState extends LogElement {
		final int _ts;
		final double _phi, _phidot;
		LogState( int ts, double phi, double phidot ){
			_ts = ts;
			_phi = phi;
			_phidot = phidot;
		}
		public String toString(){
			return "tstate="+_ts
					+" phi="+_phi
					+" phidot="+_phidot
					+"\n";
		}
		@Override
		void execute(RunState rs) {
			// do nothing
		}
	};
	static public class LogDouble extends LogElement {
		final double _d;
		final String _name;
		LogDouble( String name, double d ){
			_name =name;
			_d = d;
		}
		public String toString(){
			return _name+"="+_d+"\n";
		}
		@Override
		void execute(RunState rs) {
			// do nothing
		}
	}
	
	public synchronized void logDouble( String name, double d ){
		_logger.add( new LogDouble( name, d ));
	}
	
	public synchronized  void logRunGyro( int ts, double gy ){
		if( _logger != null )
			_logger.add( new LogGyro( ts, gy ));
		updateGyro( ts, gy );
		_deviation = _deviation * 0.6 + (_x.a0 * _params.alpha + _x.a1 + 0.005 * _lastZacc + 0.001 * last_ard._speed) * 0.4;
	}
	synchronized void updateArd(ArdState ard){
		if( last_ard != null && last_ard._pwm>0 && ard._pwm==0 )
			mode=Mode.Flying;
		last_ard = ard;
	}
	static public class ArdState extends LogElement {
		final int _ts;
		final int _steps;
		public final float _speed;
		public final float _pwm;
		final float _volt;
		final int _mode;
		public ArdState( int ts, int steps, float speed, float pwm, float volt, int mode ){
			_ts = ts; _steps=steps; _speed=speed; _pwm = pwm; _volt = volt; _mode = mode;
		}
		public String toString(){
			return String.format( "tard=%s steps=%s speed=%s pwm=%s volt=%s mode=%s\n",
									_ts, _steps, _speed, _pwm, _volt, _mode );
		}
		@Override
		public void execute(RunState rs) {
			rs.updateArd(this);
		}
	}

	public static ArdState newArdState(  int ts, int steps, float speed, float pwm, float volt, int mode ){
		return new ArdState( ts, steps, speed, pwm, volt, mode );
	}
	
	public synchronized void logRunArd(	ArdState ard ){
		_logger.add(ard);
		updateArd(ard);
	}
	static public class Logger{
		final ArrayList<LogElement> _al;
		Logger(){
			_al = new ArrayList<LogElement>(10000); 
		}
		void add( LogElement a ){
			_al.add(a);
		}
		String finish(){
			StringBuilder b = new StringBuilder();
			for( LogElement e : _al ){
				b.append(e );
			}
			return b.toString();
		}
	}
	
	static public abstract class LogElement{
		abstract void execute( RunState rs );
	}
	
	public double getDeviation() {
		return 0.9*(_x.a0 * _params.alpha + _x.a1) + 0.005 * _lastZacc;
	};
	
	public static void xassert(boolean b) {
		if (!b) {
			new Exception().printStackTrace();
		}
	}
}
