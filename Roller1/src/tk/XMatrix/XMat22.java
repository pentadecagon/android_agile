package tk.XMatrix;

public class XMat22 {
	public double a00, a01, a10, a11;
	public XMat22( double a, double b, double c, double d ){
		a00=a; a01=b; a10=c; a11=d;
	}
	public XMat22 add( XMat22 r ){
		return new XMat22( a00+r.a00, a01+r.a01, a10+r.a10, a11+r.a11  );
	}
	public XVec2 dot( XVec2 r ){
		return new XVec2( a00*r.a0+a01*r.a1, a10*r.a0 + a11*r.a1 );
	}
	public XMat22 dot( XMat22 r ){
		return new XMat22 (
				a00*r.a00+a01*r.a10,
				a00*r.a01+a01*r.a11,
				a10*r.a00+a11*r.a10,
				a10*r.a01+a11*r.a11 );
	}
	public XMat22 transpose(){
		return new XMat22( a00, a10, a01, a11 );
	}
	public String toString(){
		return String.format( "([ %s , %s ],[ %s, %s ])", a00, a01, a10, a11 );
	}
	public XMat22 oneless(){
		return new XMat22( 1-a00, -a01, -a10, 1-a11 );
	}
	public double det(){
		return a00*a11-a01*a10;
	}
} // XMat22
