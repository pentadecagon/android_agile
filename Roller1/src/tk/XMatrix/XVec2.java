package tk.XMatrix;

public class XVec2 {
	public double a0;
	public double a1;
	public XVec2( double a,double b ){
		a0=a; a1=b;
	}
	public String toString(){
		return String.format( "[ %s, %s ]", a0, a1 );
	}
	public XVec2 dot( XMat22 r ){
		return new XVec2( a0*r.a00+a1*r.a10, a0*r.a01+a1*r.a11 );
	}
	public XVec2 mult( double x ){
		return new XVec2( a0*x, a1*x );
	}
	public XVec2 add( XVec2 r ){
		return new XVec2( a0 + r.a0, a1+r.a1 );
	}
	public XMat22 outer( XVec2 r ){
		return new XMat22( a0*r.a0, a0*r.a1, a1*r.a0, a1*r.a1 );
	}
} // XVec2