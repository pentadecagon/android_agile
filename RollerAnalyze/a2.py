#!python3.2
from collections import defaultdict, namedtuple
from math import *
from numpy import array, dot, outer
import matplotlib.pyplot as plt
import numpy as np
import glob
from scipy.interpolate import InterpolatedUnivariateSpline, UnivariateSpline

Params = namedtuple('Params', ['alpha', 'k3', 'noise_v', 'noise_pos', 'noise_gyro', 'qf'])

def defaultParams():
    return Params(
                    alpha = 7.5,# 6.3,
                    k3 = 0,
                    noise_v=0.3,
                    noise_pos=0.04,
                    noise_gyro=0.003,
                    qf=0
                     )
    
class Bunch:
    def __init__(self, **kwds):
        self.__dict__.update(kwds)

nfig=0
def view(yset1=[], yset2=[], xlabel='', ylabel1='', ylabel2=''):
    global nfig
    fig = plt.figure(nfig)
    nfig += 1
    colorit = iter(['b', 'r', 'c', 'm', 'k', 'g', 'b'])
    fig.subplots_adjust( left=0.1, right=0.95, bottom=0.1, top=0.95)
    ax1 = fig.add_subplot(111)
    ax1.set_xlabel(xlabel)
    labels = []
    for (t, x, l) in yset1:
        ax1.plot(t, x, colorit.__next__()+'.-')
        labels.append(l)
    ax1.set_ylabel(ylabel1, color='b')
    
    if yset2:
        ax2 = ax1.twinx()

    plt.legend(labels)

def new_params( p, **kw ):
    a=p._asdict()
    a.update( **kw )
    return Params(**a)        
        
ident2 = np.identity( 2, float )
def kalman_predict( x, P, F, Q ):
    x1 = dot(F,x) #+ Bu
    P1 = dot( F, dot(P, F.transpose())) + Q
    return x1, P1

def kalman_update( x1, P1, z, H, R ):
    y = z - dot( H, x1 )
    S = dot( H, dot( P1, H ) ) + R
    K = dot( P1, H ) * (1.0/S)
#    assert 0 <= K[0] <= 1 and 0 <= K[1] <= 1
    x2 = x1 + K * y
    P2 = dot(ident2-outer( K, H), P1)
    if y*y > 1000 * S:
        print( "alarm, y: ", y, "S: ", S )
    cost = 0.5*(log(S) + y*y/S)
    return x2, P2, cost
    
def sqr(x):
    return x*x

def kalman_step( params, x, P, dset ):
    cosh = 1.0 + sqr( params.alpha * dset.dt ) * 0.5
    F = array( [[cosh,                                  dset.dt],
                    [sqr(params.alpha)*dset.dt,       cosh]])
    dv = dset.noise * params.noise_v
    ds = params.noise_pos
    ds0 = dv*dset.dt*params.qf   # the position noise resulting from the v-noise
    dv1 = ds*dset.dt*sqr(params.alpha)*params.qf # the v-noise resulting from the position noise
    Q01 = (ds*dv1 + dv*ds0) * dset.dt;
    Q=array([[(ds*ds+ds0*ds0*1.1)*dset.dt,    Q01],
                [Q01,                     (dv*dv+dv1*dv1*1.1)*dset.dt]])
    x1, P1 = kalman_predict(x, P, F, Q)
    
    H=array([0,1])
    R=sqr(params.noise_gyro)
    
    x2,  P2, y = kalman_update(x1, P1, dset.gy, H, R)
    
    return x2, P2, y
    
def find_end(run):
    for t, acc in zip(run.tacc, run.zacc):
        if t>0.5 and abs(acc-1.75)>1.5:
            return t
    
def calculate_cost( params, run ):
    tend = find_end(run)-0.05
    print("tend=", tend)
    x=[-0.18, 0]
    P=array([[1e-4,0],[0,0]])
    cost = 0
    t0=run.tg[0]
    outphi = []
    nullparams=new_params(params, alpha=0, noise_pos=0 )
    uvec=[]
    uvecd=[]
    for tg, gy, noise, pwm in zip( run.tg, run.gy, run.noise, run.pwm ):
        if tg<0.33 or pwm != 0 or tg > tend:
            px = nullparams
        else:
            px = params
        dset=Bunch( dt=tg-t0, gy=gy, noise=noise )
        t0=tg
        x, P, y = kalman_step( px, x, P, dset )
        outphi.append(x[0])
        uvec.append(params.alpha*x[0]+x[1] )
        uvecd.append(params.alpha*sqrt(P[0,0]) + sqrt(P[1,1])+noise*params.noise_v)
        if px == params:
            cost += y
    uvecd = array(uvecd)
    uvec=array(uvec)
    view([(run.tg, run.phi*10, 'p0*10'),
          (run.tg, array(outphi)*10, 'p1*10'),
          (run.tg, run.gy, 'gy'),
          (run.tg, uvec, 'uvec'),
#          (run.tg, (uvec-uvecd), 'uvec1'),
          (run.t, run.steps/32.0, 'steps/32'),
          (run.t, run.pwm*0.05, 'pwm'),
#          (run.tacc, run.zacc, 'acc')
          ])
    
    return cost
    

def readRun(fname):
    ''' read a dataset from a give filename'''
    ldict = defaultdict(list)
    t0=None
    last_t = 0
    accnoise = 0
    pwmnoise = 0
    for d in open(fname).read().split():
        a, b = d.split('=')
        if a == 'tg':
            b=float(b) * 1e-6
            if t0 is None:
                t0=b
            b-=t0
            last_t = b
            ldict['noise'] .append( sqrt( accnoise + pwmnoise ) )
        elif t0 is None:
            continue
        elif a == 'tacc':
            b=float(b) * 1e-3 - t0
        elif a == 't':
            b=float(b) * 1e-3 - t0
        elif a == 'kt':
            b=float(b) - t0
        elif a == 'zacc':
            accnoise = accnoise * 0.6 + 0.4* sqr( float(b)-1.75 )
        elif a == 'pwm':
            pwmnoise = pwmnoise * 0.8 + 0.2 * sqr( float(b) / 10 )
            
        ldict[a].append( float(b) )
    adict = dict( (x, array(y)) for (x,y) in ldict.items() )
    b = Bunch(**adict)
    return b
    accnoise0 = sqr( b.zacc - 1.75 )
    accnoise1 = accnoise0 * 1.0
    accnoise1[1:-1] = accnoise0[1:-1]*0.5 + accnoise0[:-2]*0.25 + accnoise0[2:]*0.25

    s = InterpolatedUnivariateSpline( b.tacc, accnoise1, k=2)
    accnoise3 = s(b.tg)
    
    view( [(b.tg, accnoise3, '3'), (b.tacc, accnoise0, '0'), (b.tacc, accnoise1, '1')])    
    plt.show()
    return b

def plotRun(run):
    view( [
           (run.tg, run.gy*100, "gy*100"),
           (run.tg, run.phi*100, "phi*100"),
           (run.t, run.steps, "steps"),
           (run.kt, run.kx0*100, "x0*100"),
           (run.tacc, (run.zacc-1.75)*100, "acc*100"),
           (run.t, run.pwm*0.1, "pwm"),
           ])

def plotAllRuns(runs):
    for x in runs:
        plotRun(x)
    plt.show()
    
def main():
    dirname=r'D:\android\git\android_agile\Scripts\2012-11-22/'
    runs = [readRun(x) for x in glob.glob(dirname+'22.52.22_8*')]
    print( len(runs) )
    cost = 0
    for x in runs:
        plotRun(x)
#        c=calculate_cost(defaultParams(), x)
#        print(c)
#        cost += c
#    print("cost: ", cost )
#    plotAllRuns(runs)
    plt.show()
        
    
main()

