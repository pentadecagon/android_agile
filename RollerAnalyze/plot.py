#! python 3.3
import os
import matplotlib.pyplot as plt
import datetime as dt
from math import *
import numpy
from numpy import array, dot

def view(t, yset1=[], yset2=[], xlabel='', ylabel1='', ylabel2='', shorts=[]):
    colorit = iter(['b', 'r', 'c', 'm', 'k', 'g'])
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.set_xlabel(xlabel)
    for x in yset1:
        ax1.plot(t, x, colorit.next()+'-')

    ax1.set_ylabel(ylabel1, color='b')

    for (t,x) in shorts:
        ax1.plot( t, x, 'r.-' )
    
    if yset2:
        ax2 = ax1.twinx()

        ax2.set_ylabel(ylabel2, color='r')
        for tl in ax2.get_yticklabels():
            tl.set_color('r') 

        for x in yset2:
            ax2.plot(t, x, colorit.next()+'-')

    plt.show()

def skey(s):
    if not '_' in s:
        return (s, 0)
    a, b = s.split('_')
    return (a, int(b))
# 100.6 -169 68.5 -38  32.1 -131
def read_lists():
    dirname=dt.date.today().isoformat()
    files=sorted(os.listdir(dirname), key=skey)
    dat=files[-1][:8]

    tlist=[]
    xlist1=[]
    xlist2=[]
    xlist3=[]
    ylist1=[]
    ylist2=[]
    kk=[0] * 400
    for fname in [x for x in files if x.startswith(dat)]:
        for line in open(dirname+'/'+fname):
            (st, sgyro_t, sgyro_val, sphi, sphidot, sard_steps, sard_speed,
                 sard_pwm, svoltage, saccel_ang, status) = line.split()
            ht = float(st) / 1000.0
            tlist.append( ht )
            h = float(sphi)*300*6.3
            xlist1.append( h ) 
            xlist2.append( float(sgyro_val)*300 )#+float(sard_speed)/720*2.83*0.175 )
            xlist3.append( xlist1[-1]+xlist2[-1] )
            ylist1.append( float(sard_steps) )
            ylist2.append( float(sard_speed) )

    xlist2a = xlist2[:]
    for n in range(2, len(xlist2)-2):
        xlist2a[n]=(xlist2[n-2]+4*xlist2[n-1]+6*xlist2[n]+4*xlist2[n+1]+xlist2[n+2])/16
            
    view( tlist, [xlist1, xlist2,ylist1,ylist2, xlist3])
    
    

    
read_lists()


#view( tlist, [xlist, x2list, [100*(u-v) for (u,v) in zip(xlist, x2list)]], [speed1, speed4 ,v2list] )

