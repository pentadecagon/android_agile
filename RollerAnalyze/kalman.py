from math import *
import numpy as np
from numpy import array, dot, outer
import matplotlib.pyplot as plt
import os, scipy
import scipy.optimize as scopt
import datetime as dt
from collections import namedtuple

Params = namedtuple('Params', ['alpha', 'k3', 'w', 'delta', 'k4', 'dev_R', 'dev_q1', 'dev_q2', 'dev_q3'])

def view(t, yset1=[], yset2=[], xlabel='', ylabel1='', ylabel2='', shorts=[]):
    colorit = iter(['b', 'r', 'c', 'm', 'k', 'g'])
    fig = plt.figure()
    fig.subplots_adjust( left=0.1, right=0.95, bottom=0.1, top=0.95)
    ax1 = fig.add_subplot(111)
    ax1.set_xlabel(xlabel)
    labels=[]
    for (x, l) in yset1:
        ax1.plot(t, x, colorit.__next__()+'.-')
        labels.append(l)
    plt.legend(labels)

    ax1.set_ylabel(ylabel1, color='b')

    
    if yset2:
        ax2 = ax1.twinx()

        ax2.set_ylabel(ylabel2, color='r')
        for tl in ax2.get_yticklabels():
            tl.set_color('r') 

        for x in yset2:
            ax2.plot(t, x, colorit.__next__()+'-')

    plt.show()

ident2 = np.identity( 2, float )
def kalman_predict( x, P, F, Bu, Q ):
    x1 = dot(F,x) + Bu
    P1 = dot( F, dot(P, F.transpose())) + Q
    return x1, P1

def kalman_update( x1, P1, z, H, R ):
    y = z - dot( H, x1 )
    S = dot( H, dot( P1, H ) ) + R
    K = dot( P1, H ) * (1.0/S)
#    assert 0 <= K[0] <= 1 and 0 <= K[1] <= 1
    x2 = x1 + K * y
    P2 = dot(ident2-outer( K, H), P1)

    return x2, P2, y

''' The equations go like this:
    (u1, u1dot) = f(u0, u0dot, steps0, steps1, dt)
    phi1dot = phi0dot + alpha2*phi0*dt - k3 * (u1dot-u0dot)
    phi1 = phi0 + phi0dot * dt  - k3*(u1-u0-u0dot*dt)
'''

def propagate_u( params, u, udot, g0, g1, dt):
    g = (g1-g0) / dt
    et = exp( -params.delta * dt )
    co = cos(dt*params.w) * et
    si = sin(dt*params.w) * et
    
    du = u-g0
    dv = udot-g
    k =params.w*params.w + params.delta*params.delta
    
    u1 = g1 + du * co + (dv+params.delta*du) * si / params.w
    u1dot = g + dv * co - (k*du + params.delta*dv) * si / params.w
    return u1, u1dot
        
def test_u():
    tvec = []
    uvec = []
    dt=0.005
    u1, u1dot = 0, 0
    for i in range(1000):
        u1, u1dot = propagate_u( u1, u1dot, (i-1)*dt, i*dt, dt )
        tvec.append(i*dt)
        uvec.append(u1)
    view( tvec, [uvec])

def sqr(x):
    return x*x

def kalman_step( params, x0, P0, u0, u0dot, steps0, steps1, dt, gyro, t ):
    R = sqr(params.dev_R) # measurement error


    hs = sin(params.dev_q1)
    hc = cos(params.dev_q1)
    Qrot=array( [[hc, hs],[-hs,hc]])
    Qdia=array( [[sqr(params.dev_q2), 0], [0, sqr(params.dev_q3)]])
    Q = dot( dot( Qrot, Qdia), Qrot.transpose() )
        
    H = array( [0,1])
    u1, u1dot = propagate_u( params, u0, u0dot, steps0, steps1, dt )
    
    du = u1-u0-u0dot*dt
    dudot = u1dot-u0dot
#    du=dudot=0
    Bu = array( [ -params.k3*du, -params.k3*dudot ] )
    F=array( [[1, dt],
              [dt*params.alpha*params.alpha,1]])    
    
    x1, P1 = kalman_predict(x0, P0, F, Bu, Q)
    x2, P2, y = kalman_update(x1, P1, gyro, H, R )
   
    return x2, P2, u1, u1dot, y

def do_kalman( params, t_vec, gyro_vec, step_vec, phi_vec ):
    
    x = array([-0.0004, -0.004])
    P = array([[0, 0], [0, 0]])
    u = step_vec[0];
    udot = 0
    uvec = [0]
    xvec = [x[0]]
    xdotvec = [0]
    udotvec = [0]
    erg = 0
    for n in range(1, len(t_vec)):
        dt = t_vec[n] - t_vec[n-1]
        x, P, u, udot, y = kalman_step( params, x, P, u, udot, 
                    step_vec[n-1]-params.k4*xvec[n-1],
                    step_vec[n]-params.k4*xvec[n-1],
                    t_vec[n]-t_vec[n-1],
                    gyro_vec[n],
                    t_vec[n])
        
        uvec.append(u)
        xvec.append( x[0] )
        xdotvec.append( x[1])
        udotvec.append(-params.k3*udot)
        if abs(y)>100:
            y=100
        erg += y*y
        
    view(t_vec, [step_vec, uvec, 400.0*phi_vec, 400*array(xvec)], [gyro_vec, xdotvec] )
    return erg

def new_params( p, **kw ):
    a=p._asdict()
    a.update( **kw )
    return Params(**a)        
        
def default_params():
    w = 10*2*pi
    k3 = pi / 360 * 0.175
    return Params(
                    alpha = 6.3,
                    k3 = 0,
                    w = 51,#w,
                    delta = 7.5,#w / (2*pi)  * log(2.0),
                    k4 = 10,#.5/k3,
                    dev_R = 1e-4,
                    dev_q1 = 0,
                    dev_q2 = 1e-5,
                    dev_q3=1 )


def calc_error( ev, params ):
    return do_kalman( params, ev.t, ev.gyro_val, ev.steps, ev.phi)


hhpar = ( 'w', 'dev_q2', 'dev_q3')

def params2x(p):
    d = p._asdict()
    return array( [d[x] for x in hhpar])

def x2params(p,x):
    d = p._asdict()
    d.update( dict( zip(hhpar,x)))
    return Params(**d)

def inner2( params, events ):    
    erg = 0
    for ev in events:
        erg += calc_error( ev, params )
    return erg
    
    
def f_inner( x, params0, events ):

    assert len(x) == 3
    params = x2params(params0, x )
    
    erg = inner2(params, events)
    print( "x=", x, " error: ", erg )
    return erg


def callback(x):
    print( "Callback: ", x )

def optimize_parameters(events):
    params = default_params()

    erg = scopt.fmin_cg(f=f_inner,
                           x0 = params2x(params),
                           fprime=None,
                           args=(params, events),
                           gtol=1e-05,
                           epsilon=1e-5,
                           maxiter=None,
                           full_output=1, disp=1, retall=0, callback=callback)
    print(erg)

        

def skey(s):
    if not '_' in s:
        return (s, 0)
    a, b = s.split('_')
    return (a, int(b))


EventData=namedtuple('DataSet', ['t', 'gyro_t', 'gyro_val', 'phi', 'steps', 'pwm'])

def read_lists():
    dirname='../scripts/'+dt.date.today().isoformat()
    dirname='../scripts/2012-10-28'
    files=sorted(os.listdir(dirname), key=skey)
    dat=files[-1][:8]

    events=[]
    for fname in [x for x in files if x.startswith(dat)]:
        t_list=[]
        gyro_t_list=[]
        gyro_list=[]
        phi_list=[]
        step_list=[]
        pwm_list=[]
        dtt=[]
        for line in open(dirname+'/'+fname):
            (st, sgyro_t, sgyro_val, sphi, sphidot, sard_steps, sard_speed,
                 sard_pwm, svoltage, saccel_ang, status) = line.split()
            ht = float(st) / 1000.0
            t_list.append( ht )
            gyro_t_list.append( float(sgyro_t))
            gyro_list.append( float(sgyro_val))
            phi_list.append( float(sphi) )
            step_list.append( int(sard_steps))
            pwm_list.append( int(sard_pwm))
            dtt.append(float(sgyro_t) - float(st))
        view( t_list, [(step_list, 'steps'), (array(phi_list)*400, 'phi'), (array(gyro_list)*50, 'gyro'), (dtt, 'dtt')])
        events.append( EventData( array( t_list),
                                  array(gyro_t_list),
                                  array(gyro_list),
                                  array(phi_list),
                                  array(step_list),
                                  array(pwm_list)))
    return events

def test():
    a = array(([ 1.0 , 2.0 ],[ 3.0, 4.0 ]))
    b = array([ 7.0, 9.0 ])
    print( dot(a,b));
    print( dot(b,a));
    print( dot( a,a));    
                        
def main():
    events = read_lists()
#    inner2(default_params(), events)
#    optimize_parameters(events)

test()    
#main()        
        
        
        
        
        
        
        
