#include <xutil/platform.hh>
#include "runstate.hh"
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <infodir/xlog.hh>
#include <infodir/infodir.hh>
#include <tensor/sqr.hh>
#include <xutil/statistics.hh>
#include <xutil/pyplot.hh>
#include <nlopt.h>
#include <float.h>
#include <cfenv>
#pragma fenv_access (on)

using namespace boost;
using namespace boost::filesystem;

static const string g_rundirname = "D:\\android\\git\\android_agile\\Scripts\\run2\\";
static const string g_minfile = "2013-12-03_23.46";

template<class T>
double nlopt_lambda_aux( unsigned int n, const double*x, double* grad, void* v )
{
	const T& lamb = *(T*)v;
	return lamb( n, x, grad );
}

template<class T>
nlopt_opt nlopt_create_lambda( nlopt_algorithm algorithm, unsigned n, const T& t )
{
	nlopt_opt opt = nlopt_create( algorithm, n );
	nlopt_set_min_objective( opt, &nlopt_lambda_aux<T>, (void*)&t );
	return opt;
}

vector<path> find_file_names( string dirname, string minentry )
{
	vector<path> res;
	for( auto x = directory_iterator(dirname); x != directory_iterator(); ++x ){
		const string s = x->path().filename().string();
		if( is_regular_file(*x)  && ends_with( s, ".txt" ) && s > minentry )
			res.push_back( x->path() );
	}
	return res;
}

vector<const RunLog*>  load_all_runlogs()
{
	vector<const RunLog*> vrunlog;
	for (path pa : find_file_names(g_rundirname, g_minfile)){
		const RunLog* runlog = load_runlog(pa.string());
		if (runlog->v_.back()->t > 3 )
			vrunlog.push_back( runlog );
	}
	return vrunlog;
}

VPDD mul2( double f, VPDD a )
{
	for (auto& x : a){
		x.second *= f;
	}
	return a;
}

double runall( const vector<const RunLog*>& vrunlog, const RunParams& param )
{
	PROFILE_HERE;
	double totalcost = 0;
	for (const RunLog* runlog : vrunlog){
		RunState rstate( param );
		runlog->run( rstate, 0 );
		totalcost += rstate.cost;
	}
	return totalcost;
};

struct OptParam { const char* name; double x; double a; double b; };

RunParams optimize_parameters( const vector<const RunLog*>& vrunlog, RunParams param )
{
	PROFILE_HERE;
	vector<OptParam > voptparam0{
#if 1
		{ "-alpha", 7, 6, 8 },
		//"phi_back",
		//"phi_front",
		{ "-sensor_noise", 0.0014, 0.0000, 0.01 },
		{ "-np1", 0.45, 0.2, 0.95 },
		{ "-np2", 100, 0.0, 200.0 },
		{ "-np3", 5.6, 0, 10 },
		//"noisepass4", &RunParams::np4 },
		{ "-np5", 2.3, 0, 10 },
		{ "-fgy1", 0.9, -1, 1 },
		{ "-ffgy1", -0.5, -1, 1 },
		{ "-fffgy1", -0.1, -1, 1 },
		//		{ "fgy3", 1, 0, 1.1 },
//		{ "ffgy3", -0.4, -1, 0 },
		//		{ "dphi", 0.07, 0.0001, 0.1 },
//		{ "init_noise", 0.02, 0.0001, 0.1 },
		{ "-motork", 0.000, 0.00, 0.1 },
		{ "-filter_gy", 0.5, 0, 0.5 },
		{ "-filter_zacc", 0.4, 0, 0.5 },
		{ "-filter_speed", 0.064, 0.05, 0.5 },
		{ "-zacc_midval", 2.33, 2, 3 }
		//"relaxtime" ,
		//		 "zacc_midval", &RunParams::zacc_midval },
#endif
    };
	vector<OptParam > voptparam;
	for (auto x : voptparam0 ){
		if (x.name[0] != '-'){
			voptparam.push_back( x );
			param[x.name] = x.x;
		} else {
			param[x.name + 1] = x.x;
		}
	}
	const int N = voptparam.size();
	const double maxdelta = 10;
	if ( N == 0)
		return param;
	int counter = 0;
	cout << setprecision( 3 );
	auto costfun = [&]( int n, const double* x, double* grad ){
		rassert( n == N );
		for ( int k = 0; k < N; ++k ){
			param[voptparam[k].name] = x[k];
		}
		cout << setw( 6 ) << counter << ' ';
		for (int k = 0; k < N; ++k){
			cout << voptparam[k].name << '=' << setw(6) << x[k] << ' ';
		};
		double erg = runall( vrunlog, param );
		if (counter % 1 == 0){
			cout << lround(erg) << endl;
		}
		counter++;
		return erg;
	};

//	nlopt_opt opt = nlopt_create_lambda( NLOPT_GN_DIRECT_L, N, costfun );
	nlopt_opt opt = nlopt_create_lambda( NLOPT_LN_BOBYQA, N, costfun );
	nlopt_set_xtol_rel( opt, 1e-5 );
	double minf = 1e27;

	vector<double> vx( N );
	vector<double> vxmin( N );
	vector<double> vxmax( N );
	for (int k = 0; k<N; ++k){
		vx[k] = voptparam[k].x;
		vxmin[k] = voptparam[k].a;
		vxmax[k] = voptparam[k].b;
		rassert( vxmin[k] <= vx[k] && vx[k] <= vxmax[k] );
	}
	nlopt_set_lower_bounds( opt, &vxmin.front() );
	nlopt_set_upper_bounds( opt, &vxmax.front( ) );
	const nlopt_result ro = nlopt_optimize( opt, &vx.front( ), &minf );
	assert( ro>0 );

	LOGI( "optimized: ======= " << minf);
	for (int k = 0; k < N; ++k){
		param[voptparam[k].name] = vx[k];
		LOGI( voptparam[k].name << '=' << vx[k] );
	}
	return param;
}

void analyze()
{ 
	PROFILE_HERE;
	vector<const RunLog*> vrunlog =  load_all_runlogs();
	RunParams opt = optimize_parameters( vrunlog, RunParams::default_params( ) );
	vector<pair<double, double>> vp1, vp2;
	double tc = 0;
	for( const RunLog* runlog : vrunlog ){
		RunState rstate( opt );
		RunResults rr;
		runlog->run( rstate, &rr );
//		const double runtime = rstate.t_touchdown - rstate.t_takeoff;
//		auto p = make_pair( runlog->startspeed_, 1.0 / runtime );
//		( runlog->startspeed_ > 0 ? vp1 : vp2 ).push_back(p);
		const string& fname = path(runlog->fname_).filename().string() + ".py";
		PyPlotter plt( fname );
		plt.label( "speed/200" ).plot2( mul2( 0.005, runlog->find_all_speed( ) ) );
		plt.label( "gy" ).plot2( runlog->find_all_gy( ) );
		plt.label( "pwm/100" ).plot2( mul2( 0.01, runlog->find_all_pwm() ) );
		plt.label( "steps500" ).plot2( mul2( 0.002, runlog->find_all_steps( ) ) );
		plt.label( "psidot" ).plot2( rr.vtpsidot );
		plt.label( "psi" ).plot2( mul2( 6.81, rr.vtpsi ) );
//		plt.label( "phitotal" ).plot2( rr.vstepspeed );
		plt.label( "delta" ).plot2( rr.vdelta );
		plt.label( "outer" ).plot2( rr.vtouter );
		//		plt.label( "delta0" ).plot2( rr.vdelta0 );
//		plt.label("zacc").plot2( mul2(0.1, runlog->find_all_zacc() ) );
		tc += rstate.cost;
		PyPlotter pzacc( "zacc.py" );
		auto pp = linear_regression( rr.vzacc );
		pzacc.plot2( VPDD{ { -0.1, -pp.first*0.1 + pp.second }, { 0.1, pp.first*0.1 + pp.second } } );
		pzacc.display_ = ".";
		pzacc.plot2( rr.vzacc ); 
		PyPlotter plotbal2( "bal2.py" );
		plotbal2.display( "." ).label("opp").plot2( rr.vbalance[0] );
		plotbal2.display( "." ).label("same").plot2( rr.vbalance[1] );
		VPDD hp2;
		for (auto x : rr.vbalance[1]){
			hp2.push_back( make_pair( abs( x.first ), abs( x.second ) ) );
		}
		auto p2 = linear_regression( hp2 );
		plotbal2.plot2( VPDD{ { 0, p2.second }, { 50, p2.first * 50 + p2.second } } );
		plotbal2.plot2( VPDD{ { 0, -p2.second }, { -50, -p2.first * 50 - p2.second } } );
		VPDD hp3;
		for (auto x : rr.vbalance[0]){
			hp3.push_back( make_pair( abs( x.first ), abs( x.second ) ) );
		}
		auto p3 = linear_regression( hp3 );
		plotbal2.plot2( VPDD{ { 0, p3.second }, { 50, p3.first * 50 + p3.second } } );
		plotbal2.plot2( VPDD{ { 0, -p3.second }, { -50, -p3.first * 50 - p3.second } } );
		cout << "reg: " << rstate.zacc_reg.run() << endl;

	}
	PyPlotter plt("speed.py");
	LOGI( "cost=" << tc );
	plt.plot2( vp1 );
	plt.plot2( vp2 );
}
#include <random>
int main( int ac, char** av )
{



	init_infodir( "D:/x/infodir2", ac, av );
	set_prof();
	analyze();
	finish_infodir( "done" );
}

/*
At any time we have a probability distribution over state.  This can be represented as the
sum of individual states
with each having a probabilit assigned to it.  Or as sum of gaussian.

The state is a vector of real / discrete values at each time.

At each new measurement we are faced with the problem:

What's the current distribution, given the previous distribution, and the measured value?

A distribution D consists of:
1. Fixed Scalar.
2. Scalars with a discrete number of options, each with P
3. Gaussian scalars.
4. Mixture of Gaussian scalars.
5. Independent combination of scalars.

Why smoothing: In order to decide about actions to be taken smoothing is not particularly useful.
We need smoothing to discover additional symmetries, like occasional parameter changes.
We could track all possible parameter changes, but that would required a rather complex state matrix.
So smoothing is required to simplify the state matrix.  We just don't want it to track all options simultaneously.

Optimized system:

- set parameters
- initial state
- measurement, cost = p(measurement)
- maybe modify state, cost=cost(modification)

A program that reproduces the measurements.  We need to store that program.
State needs to be complete, each step operates ont he state.  
The optimizer, however, sees all states simultaneously.
Store the program.  The program is a series of function calls.  Need to store which function to call.
One parameter is always the state.  The other parameters must be stored.  We represent each function
call as a class.  The class type says which function to call.  The parameter are stored as well.
The class is able to calculate the cost depending on the previous state.  So we have a cost of storing
the class type, and a cost of storing the parameters.

*/