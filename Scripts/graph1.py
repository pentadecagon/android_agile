#! python3.2
import os
import matplotlib.pyplot as plt
import datetime as dt
from math import *
import numpy
from numpy import array, dot
from collections import defaultdict
from threading import Thread


nfig=0
def view(yset1=[], yset2=[], xlabel='', ylabel1='', ylabel2='', shorts=[]):
    global nfig
    plt.figure(nfig)
    fig = plt.figure(nfig)
    nfig += 1
    colorit = iter(['b', 'r', 'c', 'm', 'k', 'g'])
    fig.subplots_adjust( left=0.1, right=0.95, bottom=0.1, top=0.95)
    ax1 = fig.add_subplot(111)
    ax1.set_xlabel(xlabel)
    labels = []
    for (t, x, l) in yset1:
        ax1.plot(t, x, colorit.__next__()+'.-')
        labels.append(l)

    ax1.set_ylabel(ylabel1, color='b')


    for (t,x) in shorts:
        ax1.plot( t, x, 'r.-' )
    
    if yset2:
        ax2 = ax1.twinx()

        ax2.set_ylabel(ylabel2, color='r')
        for tl in ax2.get_yticklabels():
            tl.set_color('r') 

        for (x,l) in yset2:
            ax2.plot(t, x, colorit.__next__()+'-')
            labels.append(l)
    plt.legend(labels)


ident3 = numpy.identity( 3, float )
def kalman_step3( P, x, dt, z ): # P=2x2, x=2x1, dt=ms
    # returns P, x

    Q0v=0.025
    Q0a=0.000
    Q0s=0.0
    dtmax=10
    h = dt / (1+dt/dtmax)
    da=Q0a*h
    dv=Q0v*h
    dx=Q0s*h
    qv=array( [dv*dt/1.5, dv, 0] )
    qa=array( [da*dt*dt/8, da*dt/2, da] )
    Q = numpy.outer(qv,qv) + numpy.outer(qa,qa)
    Q[0,0] += dx * dx
    
    F = array(((1, dt, dt*dt/2),(0,0.99,dt),(0,0,1)))
    
    x1 = dot(F,x)

    P1 = dot( F, dot(P, F.transpose()) ) + Q

    H = array( (1,0,0) )

    y = z - dot( H, x1 )

    R = 0.16 # measurement error
    S = dot( H, dot( P1, H ) ) + R

    K = dot( P1, H ) * (1.0/S)

    x2 = x1 + K * y
    
    P2 = dot(ident3-numpy.outer( K, H), P1)

    return y, P2, x2


ident2 = numpy.identity( 2, float )
def kalman_step( P, x, dt, z ): # P=2x2, x=2x1, dt=ms
    # returns P, x

    Q0=0.005
    dtmax=10
    
    dv=Q0*dt / (1+dt/dtmax)  #  it takes 25 ms to reach speed=1
    dvq=dv*dv
    Q = ((dt*dt/2*dvq, dt/2*dvq),(dt/2*dvq, dvq))

    F = array(((1, dt),(0,1)))
    
    x1 = dot(F,x)
    
    P1 = dot( F, dot(P, F.transpose()) ) + Q

    H = array( (1,0) )

    y = z - dot( H, x1 )

    R = 0.16 # measurement error
    S = dot( H, dot( P1, H ) ) + R

    K = dot( P1, H ) * (1.0/S)

    x2 = x1 + K * y
    
    P2 = dot(ident2-numpy.outer( K, H), P1)

    return y, P2, x2


tlist=[]
xlist=[]
def read_lists():
    for u in open("d:/android/motor.log"):
        if u.strip():
            a, b = map( int, u.strip().split() )
            tlist.append(a*0.001)
            xlist.append(b)

#read_lists()


    
#x2list, v2list = kalman3()

#speed4 = [0, 0]+[(xlist[n+2]-xlist[n-2])/(tlist[n+2]-tlist[n-2]) for n in range(2,len(tlist)-2)]+[0,0]
#speed1 = [0]+[(xlist[n]-xlist[n-1])/(tlist[n]-tlist[n-1]) for n in range(1,len(tlist))]




def skey(s):
    if not '_' in s:
        return (s, 0)
    a, b = s.split('_')
    return (a, int(b))
# 100.6 -169 68.5 -38  32.1 -131
def read_lists():
    dirname=sorted( [x for x in os.listdir('.') if x.startswith('2012')] )[-1]
    print(dirname)
#    dirname='2012-11-11'
    files=sorted(os.listdir(dirname), key=skey)
    dat=files[-1][:8]

    for fname in [x for x in files if x.startswith(dat)]:
        ldict = defaultdict(list)
        noise = 0
        for d in open(dirname+'/'+fname).read().split():

            a, b = d.split('=')
            ldict[a].append( float(b) )
            if a=='pwm':
                noise = noise * 0.9 + float(b)*float(b) * 0.1
            if a=='speed':
                noise = noise * 0.9 + float(b)*float(b) * 0.1
            if a == 'tg':
                ldict['noise'].append(sqrt(noise))

            

            adict = dict( (x, array(y)) for (x,y) in ldict.items() )
        view(  [ (adict['tg']*1e-3, adict['gy']*100+adict['noise']*0.1, 'gy+noise'),
                   (adict['tg']*1e-3, adict['gy']*100, '100*gyro'),
                   (adict['t'], adict['steps'], 'steps'),
                   (adict['t'], adict['speed']*0.1, 'speed*0.1'),
                   (adict['t'], adict['pwm']*0.1, '0.1*pwm'),
                    (adict['tacc'], adict['zacc']*10, 'acc*10')
                  ])    
    
read_lists()
plt.show()


#view( tlist, [xlist, x2list, [100*(u-v) for (u,v) in zip(xlist, x2list)]], [speed1, speed4 ,v2list] )

