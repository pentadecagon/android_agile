
#include <Wire.h>
//Servo is using Timer3 so we can't use this library
//#include <Servo.h> 

#include <Max3421e.h>
#include <Usb.h>
#include <AndroidAccessory.h>
#include <LiquidCrystal.h>



#define LEDREDPIN 4
#define LEDGREENPIN 9
#define LEDYELLOWPIN 5 //analogwrite to this pin will override the setting for timer3
#define LEDINFO0 22 // even numbers are green
#define LEDINFO1 23 // odds are red
#define LEDINFO2 24
#define LEDINFO3 25
#define LEDINFO4 26
#define LEDINFO5 27
#define LEDINFO6 28
#define LEDINFO7 29
#define BTN0 15 
#define BTN1 14
#define BTN2 EXT2
#define BTN3 EXT4


#define EXT1 GND
#define EXT2 17
#define EXT3 GND
#define EXT4 16
#define EXT5 39
#define EXT6 38
#define EXT7 30
#define EXT8 31
#define EXT9 AP8
#define EXT10 AP9
#define EXT11 AP14
#define EXT12 AP15
#define EXT13 47
#define EXT14 +3V3
#define EXT15 GND
#define EXT16 +5V

#define LCD_E 32
#define LCD_D4 37
#define LCD_D5 36
#define LCD_D6 35
#define LCD_D7 34
#define LCD_RS 33

#define X0_TACHO0 21
#define X0_TACHO1 20
#define X0_PWMH0 7
#define X0_DIRL0 40
#define X0_PWMH1 8
#define X0_DIRL1 41
#define X0_STATUS 10

#define X1_TACHO0 19
#define X1_TACHO1 18
#define X1_PWMH0 45
#define X1_DIRL0 49
#define X1_PWMH1 44
#define X1_DIRL1 48
#define X1_STATUS 11

#define X2_TACHO0 2
#define X2_TACHO1 3
#define X2_PWMH0 6
#define X2_DIRL0 43
#define X2_PWMH1 46
#define X2_DIRL1 42
#define X2_STATUS 12


#define speed_array_size 16u
#define speed_mask 15u
// speed_array is a ring buffer.  norm_index normalizes the array index
#define norm_index(x) ( (x) & speed_mask )

#define print2(a,b) Serial.print(a); Serial.println(b)
#define print3(a,b,c) Serial.print(a); Serial.print(' '); Serial.print(b); ; Serial.print(' '); Serial.println(c)
#define print4(a,b,c,d) Serial.print(a); Serial.print(b); Serial.print(c); Serial.println(d)

SIGNAL(TIMER3_COMPA_vect)
{
	TIMSK3 &= ~(_BV(OCIE3A));  //switch off interrupt
	TCCR3B = 0; //Timer off
	
	//here the things to do...

	print2("A=", micros() );		
};

SIGNAL(TIMER3_COMPB_vect)
{
	TIMSK3 &= ~(_BV(OCIE3B));  //switch off interupt
	TCCR3B = 0; //Timer off
	//here the things to do...

	print2("B=", micros() );	
};

SIGNAL(TIMER3_COMPC_vect)
{
	TIMSK3 &= ~(_BV(OCIE3C));  //switch off interupt
	TCCR3B = 0; //Timer off
	//here the things to do...

	print2("C=", micros() );	
};

void init_timer3()
{
	TCCR3A = 0; //normal operation, no PWM
	TCCR3B = 0; //Timer off
	TCCR3C =  0; //no output
	TIMSK3 = 0; //no interrupts
	TIFR3 = _BV(ICF3) | _BV(OCF3C) |  _BV(OCF3B)  | _BV(OCF3A) | _BV(TOV3);
};

// the time span must be given in units of 16us
void start_timer3_A(uint16_t t_16us)
{
	OCR3A = t_16us;  //timer compare value
	TIMSK3 |= _BV(OCIE3A); //interrupt on
	TCNT3 = 0; //reset counter
	TCCR3B = _BV(2); //Timer on prescale 256 
}

// the time span must be given in units of 16us
void start_timer3_B(uint16_t t_16us)
{
	OCR3B = t_16us;  //timer compare value
	TIMSK3 |= _BV(OCIE3B); //interrupt on
	TCNT3 = 0; //reset counter
	TCCR3B = _BV(2); //Timer on prescale 256 
}

// the time span must be given in units of 16us
void start_timer3_C(uint16_t t_16us)
{
	OCR3C = t_16us;  //timer compare value
	TIMSK3 |= _BV(OCIE3C); //interrupt on
	TCNT3 = 0; //reset counter
	TCCR3B = _BV(2); //Timer on prescale 256 
}



unsigned int g_max_speed_start_time=0;

LiquidCrystal lcd( LCD_RS, LCD_E, LCD_D4, LCD_D5, LCD_D6, LCD_D7 );

enum State { State_Normal=0,
	State_Target_Pos,  // go to defined position and then stop actively
	State_Target_Speed,  // go to defined speed and then stop actively
	State_Target_Time
};

enum Mode { Mode_Forward=0, Mode_Backward=1, Mode_Off=2, Mode_Uninit=3, Mode_Stop=4, Mode_Max_Forward, Mode_Max_Backward }; 

struct NxtMotor {
	const int pwmh0_pin, pwmh1_pin, l0_pin, l1_pin, tacho0_pin, tacho1_pin, status_pin;
	const int tacho_leds[2];
	int step;
	Mode mode; 
	uint8_t pwm;  // latest set pwm value
	int8_t cur_dir; // current direction as determined from the interrup. 0=forward, 1=backward
	int8_t last_interrupt;  // 0 or 1
	volatile uint32_t speed_array[speed_array_size];
	volatile uint8_t speed_array_pos;
	uint8_t has_started; // will be set to 1 during the first interrupt
	uint16_t max_speed;
	int16_t target_pos;
        unsigned long target_time;
        unsigned long target_time_start;
	State state;
	int last_lcd_step;
	void init();
};

NxtMotor motor1 = { X0_PWMH0, X0_PWMH1, X0_DIRL0, X0_DIRL1, X0_TACHO0, X0_TACHO1, X0_STATUS, LEDINFO2, LEDINFO3 };
NxtMotor motor2 = { X1_PWMH0, X1_PWMH1, X1_DIRL0, X1_DIRL1, X1_TACHO0, X1_TACHO1, X1_STATUS,  LEDINFO4, LEDINFO5 };

void motor_off( struct NxtMotor& m )
{
	if( m.mode == Mode_Off )
		return;
	digitalWrite( m.l0_pin, LOW );
	digitalWrite( m.l1_pin, LOW );
	digitalWrite( m.pwmh0_pin, LOW );
	digitalWrite( m.pwmh1_pin, LOW );
	m.mode = Mode_Off;
	m.pwm = 0;
}

void stop_motor( struct NxtMotor& m )
{
	if( m.mode == Mode_Stop )
		return;
	digitalWrite( m.pwmh0_pin, LOW );
	digitalWrite( m.pwmh1_pin, LOW );
	digitalWrite( m.l0_pin, HIGH );
	digitalWrite( m.l1_pin, HIGH );
	m.mode = Mode_Stop;
	m.pwm = 0;
}

void set_pwm(struct NxtMotor& m, bool direction, uint8_t pwm)
{
	if( m.mode != direction ){
		motor_off(m);
		digitalWrite( direction ? m.l1_pin : m.l0_pin, 1 );
		m.mode=(Mode)direction;
	} else if( m.pwm == pwm ){
		return;
	}
	analogWrite( direction ? m.pwmh0_pin : m.pwmh1_pin, pwm );
	m.pwm = pwm;
}

void set_motor_max( struct NxtMotor& m, bool direction )
{
	motor_off(m);
	if( direction ){ // backward
		digitalWrite( m.l1_pin, HIGH );
		digitalWrite( m.pwmh0_pin, HIGH );
		m.mode = Mode_Max_Backward;
	} else {
		digitalWrite( m.l0_pin, HIGH );
		digitalWrite( m.pwmh1_pin, HIGH );
		m.mode = Mode_Max_Forward;
	}
}
		

void stop_actively()
{
	stop_motor( motor1 );
	stop_motor( motor2 );
}

void update_lcd_val( struct NxtMotor& m, int row )
{
	int step = m.step;
	if( step != m.last_lcd_step ){
		char text[8];
		snprintf( text, 7, "%6i", step ); 
		lcd.setCursor( 0, row );
		lcd.print( text );
		m.last_lcd_step = step;
	}
}

void update_lcd()
{
	update_lcd_val( motor1, 0 );
	update_lcd_val( motor2, 1 );
}

void NxtMotor::init(){
	pinMode(tacho0_pin, INPUT_PULLUP);
	pinMode(tacho1_pin, INPUT_PULLUP); 
	pinMode(pwmh0_pin, OUTPUT);
	pinMode(pwmh1_pin, OUTPUT);
	pinMode(l0_pin, OUTPUT);
	pinMode(l1_pin, OUTPUT);
	pinMode(status_pin, INPUT_PULLUP);
	mode=Mode_Uninit;
	motor_off(*this);
	step=0;
	memset( (void*)&speed_array, 1, sizeof(speed_array) );
	cur_dir=last_interrupt=2;
	has_started=0;
	max_speed = target_pos = 0;
	state = State_Normal;
	last_lcd_step = -100;
}

// error_code=4..11:  inconsistent interrupt
// error_code=18: invalid command byte
// error_code=19: bad delay

volatile int8_t error_code = 0;

void set_error( uint8_t e )
{
	error_code = e;
	print2("Error: ", e );
}

void handle_max_speed( struct NxtMotor& motor, uint32_t t )
{
  Serial.print("check max speed");
	const uint8_t n = norm_index(motor.speed_array_pos-5) ;
	const uint32_t t0 = motor.speed_array[n];
	if( (t-t0)/4 < motor.max_speed  && (t & 1) == 0 ){
		print2("Max speed: ", (t-t0)/4 );
		stop_actively();
		motor.state = State_Normal;
	}
}

void tacho_interrupt(struct NxtMotor* motor, uint8_t id /* id identifies the interrupt triggered */ )
{
	const byte tacho0 =  digitalRead(motor->tacho0_pin);
	const byte tacho1 =  digitalRead(motor->tacho1_pin);
	const uint8_t t = tacho0 ^ tacho1 ^ id;
	digitalWrite(motor->tacho_leds[id], tacho0 );

	if( t != motor->cur_dir ){ // direction changed
		motor->cur_dir = t;
		if( motor->last_interrupt != id ){ // check consistency
			if( motor->has_started ) {  
				set_error( 4+2*t+id );
			} else {
				motor->has_started = 1;
			}
		}
		// invalidate speed array if direction changed
		memset((void*)&motor->speed_array, 1, sizeof(motor->speed_array) );
	} else { // direction did not change
		if( motor->last_interrupt == id )
			set_error( 8+2*t+id );
	}
	if(t) 
		motor->step--;
	else
		motor->step++;
	motor->last_interrupt = id;

	const uint32_t m = micros();
	motor->speed_array[norm_index(motor->speed_array_pos)] = m;
	motor->speed_array_pos++;

	switch( motor->state ){
	case State_Target_Speed:
		handle_max_speed(*motor, m);
		break;
	case State_Target_Pos:
		if( motor->target_pos == motor->step ){
			print2( "target pos reached: ", motor->target_pos );
			motor->target_pos = 0;
			stop_actively(); 
		}
		break;
	}

}

uint16_t get_speed(struct NxtMotor& motor)
{
	const uint8_t n2 = norm_index(motor.speed_array_pos-1u);
	const uint32_t t2 = motor.speed_array[n2];
	if( t2 & 1u ){
		return 0;
	}
	uint32_t erg = 0x10000;
	const uint32_t t1 = motor.speed_array[norm_index(n2-4u)];
	if( (t1 & 1u) == 0 ){
		erg = (t2-t1)/4;
	}
	const uint32_t t1a = motor.speed_array[norm_index(n2-2u)];
	if( (t1a & 1u) == 0 ){
		const uint32_t e1 = (t2-t1a);
		if( e1 < erg )
			erg = e1 / 2;
	}
	const uint32_t t1b = motor.speed_array[norm_index(n2-1u)];
	if( (t1b & 1u) == 0 ){
		const uint32_t e2 = t2-t1b;
		if( 2 * e2 < erg )
			erg = e2;
	}
	const uint32_t h = micros()-t2;
	if( h > erg)
		erg = h;

	if( erg > 0xffff ) {
		return 0;
	} else
		return erg;
}

AndroidAccessory acc("Roller1", "Roller1", "Roller1", "0.1", "", "0000000049247582");

void setup();
void loop();

void init_leds()
{
	pinMode(LEDREDPIN, OUTPUT);
	pinMode(LEDGREENPIN, OUTPUT);
	pinMode(LEDYELLOWPIN, OUTPUT);

	 //infoleds
	for(uint8_t i = 22; i<30; i++){
		pinMode(i, OUTPUT);
		digitalWrite(i, HIGH);
	}

	delay(500);		
	for(uint8_t i = 22; i<30; i++){
		digitalWrite(i, LOW);
	}
}

void X1_T0C(){ tacho_interrupt(&motor1, 0); }
void X1_T1C(){ tacho_interrupt(&motor1, 1); }
void X2_T0C(){ tacho_interrupt(&motor2, 0); }
void X2_T1C(){ tacho_interrupt(&motor2, 1); }

void setup()
{
	Serial.begin(115200);
	Serial.print("Setup... ");
	init_leds();
	acc.powerOn();
	motor1.init();
	motor2.init();
	attachInterrupt(2, X1_T0C, CHANGE);
	attachInterrupt(3, X1_T1C, CHANGE);
	attachInterrupt(4, X2_T0C, CHANGE);
	attachInterrupt(5, X2_T1C, CHANGE);
	pinMode(BTN0, INPUT_PULLUP);
	pinMode(BTN1, INPUT_PULLUP);
	lcd.begin(16, 2);
	lcd.print("JK LCD");
	analogReference(INTERNAL1V1);
	init_timer3();
//	start_timer3_B( 62500U );
	Serial.println("Done!");
}


struct OutMsg {
	int16_t voltage;
	uint16_t speed;
	int16_t step1;
	int16_t step2;
	uint8_t check_byte;
	uint8_t cur_dir;
	uint8_t error_code;
	uint8_t pwm;
	uint8_t mode;
	uint8_t x14; // reserved, to round the size to 16
	uint8_t x15; // reserved, to round the size to 16
	uint8_t x16; // reserved, to round the size to 16
};

void update_max_speed()
{
	if( motor1.state == State_Target_Speed && motor1.pwm < 255 ){
		noInterrupts();
		if( motor1.state == State_Target_Speed ){
			unsigned int h = millis() - g_max_speed_start_time;
			uint8_t newpwm;
			if( h >= 120 )
				newpwm = 255;
			else
				newpwm = 2*h+15;
			print2( "update max speed: ", newpwm );
			set_pwm( motor1, motor1.mode, newpwm );
			set_pwm( motor2, motor2.mode, newpwm );
		}
		interrupts();
	}
}

void check_buttons()
{
	if(digitalRead(BTN0) == LOW){
		digitalWrite( motor1.l0_pin, 1 );
		digitalWrite( motor1.pwmh0_pin, 1 );
		motor1.mode = Mode_Uninit;
	} else {
		motor_off( motor1 );
	}
	digitalWrite( LEDGREENPIN, digitalRead( motor1.status_pin ) );
}

void check_max_time()
{
	if (motor1.state != State_Target_Time)
		return;
        long t = millis();
	if( t >= motor1.target_time ){
		noInterrupts();
	        stop_actively();
		motor1.state = motor2.state = State_Normal;
		interrupts();
		return;
	} else {
		long dt = t - motor1.target_time_start;
		int newpwm = dt * 2;
		set_pwm( motor1, motor1.mode, newpwm );
		set_pwm( motor2, motor2.mode, newpwm );
	}
}

void loop()
{
	delay( 1 );
	update_max_speed();
	update_lcd();
	check_max_time();
	if ( ! acc.isConnected() ){
		digitalWrite(LEDREDPIN, (millis()>>7) & 1);
		motor_off(motor1);
		motor_off(motor2);
		motor1.state = motor2.state = State_Normal;		
		delay(5); // maybe save some power
		digitalWrite(LEDREDPIN, 0);
 		return;
	}
	byte msg[4]; // 0->command, 1->direction, 2->pwm, 3->check byte

	if ( 0 >= acc.read(msg, sizeof(msg), 1) ){
            static unsigned int yellow_count = 0;
            digitalWrite(LEDYELLOWPIN, (yellow_count >> 7) & 1);
            ++yellow_count;
            return;
	}
        static unsigned int green_count = 0;
	digitalWrite(LEDGREENPIN, (green_count>>3) & 1);
        ++green_count;

	if( msg[0] > 0 ){
		Serial.print("Command: ");
		Serial.print( msg[0] ); Serial.print(", ");
		Serial.print( msg[1] ); Serial.print(", ");
		Serial.println( msg[2]);
	} 

	noInterrupts();

	switch(msg[0]){
	case 0: // just for returning values
		break;
	case 1: // set given direction=msg[1] and pwm=msg[2]
		motor1.state = motor2.state = State_Normal;
		set_pwm( motor1, msg[1], msg[2] );
		set_pwm( motor2, msg[1], msg[2] );
		break;
	case 9: // reset and switch off motors
		motor1.step=motor2.step=0; 
	case 10: // switch off motors
		motor1.state = motor2.state = State_Normal;
		motor_off(motor1);
		motor_off(motor2);
		break;
	case 12: { // set max speed 
                int16_t speed0 = *(int16_t*)&msg[1];
		motor1.max_speed = abs(speed0);
                print2("max speed=", motor1.max_speed );
                byte newdir = speed0<0;
                set_pwm(motor1, newdir, 15);
                set_pwm(motor2, newdir, 15);
		motor1.state = motor2.state = State_Target_Speed;
		g_max_speed_start_time=millis(); 
            }
		break;
	case 13: // set target pos
		motor1.target_pos = motor1.step + *(int16_t*)&msg[1];
		motor1.state = State_Target_Pos;
		break;
	case 14: // motor1
		set_pwm( motor1, msg[1], msg[2] );
		break;
	case 15:
		set_pwm( motor2, msg[1], msg[2] );
		break;
	case 18:{
		int16_t dt = *(int16_t*)&msg[1];
		print2( "Target time=", dt );
		byte newdir = (dt < 0);
		long t = millis();
		if( abs(dt) > 64 )
		    break;
		motor1.target_time = motor2.target_time = t + abs(dt);
		motor1.target_time_start = motor2.target_time_start = t - 64;
//                print2( "Target time=", motor1.target_time )
//                ptint2( "dt=", dt )
		motor1.state = motor2.state = State_Target_Time;
		set_pwm( motor1, newdir, 128 );
		set_pwm( motor2, newdir, 128 );
		break;}

	default: // invalid command byte
		set_error(18);
	}

	OutMsg outmsg;
	outmsg.speed = get_speed(motor1);
	outmsg.step1 = motor1.step;
	outmsg.step2 = motor2.step;
	outmsg.cur_dir = motor1.cur_dir;
	outmsg.error_code = error_code;
	outmsg.pwm = motor1.pwm;
	outmsg.mode = motor1.mode;
	error_code = 0;
	interrupts();
	outmsg.check_byte = msg[3];
	outmsg.voltage = analogRead(A0);
	acc.write( &outmsg, sizeof(outmsg) );
}

