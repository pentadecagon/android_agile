// include the library code:

//LEGO Shield V1
#include <HardwareSerial.h>


#define LEDREDPIN 4
#define LEDGREENPIN 9
#define LEDYELLOWPIN 5
#define LEDINFO0 22 // even numbers are green
#define LEDINFO1 23 // odds are red
#define LEDINFO2 24
#define LEDINFO3 25
#define LEDINFO4 26
#define LEDINFO5 27
#define LEDINFO6 28
#define LEDINFO7 29
#define BTN0 15 
#define BTN1 14
#define BTN2 EXT2
#define BTN3 EXT4


#define EXT1 GND
#define EXT2 17
#define EXT3 GND
#define EXT4 16
#define EXT5 39
#define EXT6 38
#define EXT7 30
#define EXT8 31
#define EXT9 AP8
#define EXT10 AP9
#define EXT11 AP14
#define EXT12 AP15
#define EXT13 47
#define EXT14 +3V3
#define EXT15 GND
#define EXT16 +5V


#define X0_TACHO0 21
#define X0_TACHO1 20
#define X0_PWMH0 7
#define X0_DIRL0 40
#define X0_PWMH1 8
#define X0_DIRL1 41


#define X1_TACHO0 19
#define X1_TACHO1 18
#define X1_PWMH0 45
#define X1_DIRL0 49
#define X1_PWMH1 44
#define X1_DIRL1 48

#define X2_TACHO0 2
#define X2_TACHO1 3
#define X2_PWMH0 6
#define X2_DIRL0 43
#define X2_PWMH1 46
#define X2_DIRL1 42


class Motor{
  const uint8_t Tacho0;
  const uint8_t Tacho1;
  const uint8_t pwmH0;
  const uint8_t dirL0;
  const uint8_t pwmH1;
  const uint8_t dirL1;
  uint8_t dir;
  public:
  volatile int16_t steps;
  Motor(uint8_t T0, uint8_t T1, uint8_t pH0, uint8_t dL0, uint8_t pH1, uint8_t dL1):
    Tacho0(T0),Tacho1(T1),pwmH0(pH0),dirL0(dL0),pwmH1(pH1),dirL1(dL1), dir(0), steps(0) {};
  void init();  
  void free();  
  void stop();
  void left(uint8_t);
  void right(uint8_t);
};

Motor X0(X0_TACHO0,X0_TACHO1,X0_PWMH0,X0_DIRL0,X0_PWMH1,X0_DIRL1);
Motor X1(X1_TACHO0,X1_TACHO1,X1_PWMH0,X1_DIRL0,X1_PWMH1,X1_DIRL1);
Motor X2(X2_TACHO0,X2_TACHO1,X2_PWMH0,X2_DIRL0,X2_PWMH1,X2_DIRL1);


void X0_T0C()
{
  uint8_t t = digitalRead(X0_TACHO0);
  digitalWrite(LEDINFO2, t);  
  t ^= digitalRead(X0_TACHO1);
  if(t) X0.steps++; else X0.steps--;
}  

void X0_T1C()
{
  uint8_t t = digitalRead(X0_TACHO1);
  digitalWrite(LEDINFO3, t);      
  t ^= digitalRead(X0_TACHO0);
  if(!t) X0.steps++; else X0.steps--;
}  


void X1_T0C()
{
  uint8_t t = digitalRead(X1_TACHO0);
  digitalWrite(LEDINFO4, t);  
  t ^= digitalRead(X1_TACHO1);
  if(t) X1.steps++; else X1.steps--;
}  

void X1_T1C()
{
  uint8_t t = digitalRead(X1_TACHO1);
  digitalWrite(LEDINFO5, t);      
  t ^= digitalRead(X1_TACHO0);
  if(!t) X1.steps++; else X1.steps--;
}  


void X2_T0C()
{
  uint8_t t = digitalRead(X2_TACHO0);
  digitalWrite(LEDINFO6, t);  
  t ^= digitalRead(X2_TACHO1);
  if(t) X2.steps++; else X2.steps--;
}  

void X2_T1C()
{
  uint8_t t = digitalRead(X2_TACHO1);
  digitalWrite(LEDINFO7, t);      
  t ^= digitalRead(X2_TACHO0);
  if(!t) X2.steps++; else X2.steps--;
}  

void Motor::init()
{
 pinMode(Tacho0, INPUT_PULLUP);
 pinMode(Tacho1, INPUT_PULLUP); 
 pinMode(pwmH0, OUTPUT);
 pinMode(dirL0, OUTPUT);
 pinMode(pwmH1, OUTPUT);
 pinMode(dirL1, OUTPUT);
 free();
}

void Motor::free()
{
 digitalWrite(dirL0, LOW);   
 digitalWrite(dirL1, LOW);   
 digitalWrite(pwmH0, LOW);   
 digitalWrite(pwmH1, LOW);   
 dir=3;
}  

void Motor::stop()
{
 digitalWrite(pwmH0, LOW);   
 digitalWrite(pwmH1, LOW);   
 digitalWrite(dirL0, HIGH);   
 digitalWrite(dirL1, HIGH);   
 dir=0;
}  
  
void Motor::left(uint8_t s)
{
  if( 1 != dir ) {
    free();
    digitalWrite(dirL1, HIGH);    
    dir=1;
  }  
  analogWrite(pwmH0, s);
}  
  
  
void Motor::right(uint8_t s)
{
  if( 2 != dir ) {
    free();
    digitalWrite(dirL0, HIGH);    
    dir=2;
  }  
  analogWrite(pwmH1, s);
}  

  
void setup() {
 pinMode(LEDREDPIN, OUTPUT);
 pinMode(LEDGREENPIN, OUTPUT);
 pinMode(LEDYELLOWPIN, OUTPUT);

 //infoleds
 for(uint8_t i = 22; i<30; i++) pinMode(i, OUTPUT);
 
 pinMode(BTN0, INPUT_PULLUP);
 pinMode(BTN1, INPUT_PULLUP); 
 pinMode(BTN2, INPUT_PULLUP); 
 pinMode(BTN3, INPUT_PULLUP); 
  
 pinMode(EXT5, OUTPUT);
 pinMode(EXT6, OUTPUT);
 pinMode(EXT7, OUTPUT);
 pinMode(EXT8, OUTPUT);
 pinMode(EXT13, OUTPUT);
  
 X0.init();
/* pinMode(X0_TACHO0, INPUT_PULLUP);
 pinMode(X0_TACHO1, INPUT_PULLUP); 
 pinMode(X0_PWMH0, OUTPUT);
 pinMode(X0_DIRL0, OUTPUT);
 pinMode(X0_PWMH1, OUTPUT);
 pinMode(X0_DIRL1, OUTPUT);*/

 X1.init();
/*inMode(X1_TACHO0, INPUT_PULLUP);
 pinMode(X1_TACHO1, INPUT_PULLUP); 
 pinMode(X1_PWMH0, OUTPUT);
 pinMode(X1_DIRL0, OUTPUT);
 pinMode(X1_PWMH1, OUTPUT);
 pinMode(X1_DIRL1, OUTPUT);*/

 X2.init();
/*Mode(X2_TACHO0, INPUT_PULLUP);
 pinMode(X2_TACHO1, INPUT_PULLUP);
 pinMode(X2_PWMH0, OUTPUT);
 pinMode(X2_DIRL0, OUTPUT);
 pinMode(X2_PWMH1, OUTPUT);
 pinMode(X2_DIRL1, OUTPUT);*/

 pinMode(13,OUTPUT);
/*
 digitalWrite(X1_DIR0,0);
 digitalWrite(X1_DIR1,0);
 digitalWrite(X2_DIR0,0);
 digitalWrite(X2_DIR1,0);
 
 */

  digitalWrite(LEDREDPIN, HIGH);
  digitalWrite(LEDGREENPIN, HIGH);
  digitalWrite(LEDYELLOWPIN, HIGH);
  delay(100);
  for(uint8_t i = 22; i<30; i++){
    digitalWrite(i, HIGH);
    delay(100);
  }
  delay(1000);
  for(uint8_t i = 22; i<30; i++){
    digitalWrite(i, LOW);
    delay(100);
  }
  digitalWrite(LEDREDPIN, LOW);
  digitalWrite(LEDGREENPIN, LOW);
  digitalWrite(LEDYELLOWPIN, LOW);

  Serial.begin(115200);
  Serial.println("\r\nLEGO Start");

  attachInterrupt(2, X0_T0C, CHANGE);
  attachInterrupt(3, X0_T1C, CHANGE);
  attachInterrupt(4, X1_T0C, CHANGE);
  attachInterrupt(5, X1_T1C, CHANGE);
  attachInterrupt(0, X2_T0C, CHANGE);
  attachInterrupt(1, X2_T1C, CHANGE);

  analogReference(INTERNAL1V1);

  digitalWrite(LEDINFO0, HIGH);
  
}




void loop() {

  digitalWrite(13, !digitalRead(13));


  if(!digitalRead(BTN0) && !digitalRead(BTN1)){
    X0.stop();
    X1.stop();
    X2.stop();
  }
  else
  if(!digitalRead(BTN0)){
    X0.left(100);
    X1.left(100);
    X2.left(100);
  }
  else
  if(!digitalRead(BTN1)){
    X0.right(100);
    X1.right(100);
    X2.right(100);
  }
  else{
    X0.free();
    X1.free();
    X2.free();
  }
  digitalWrite(LEDYELLOWPIN, !digitalRead(BTN0));
  digitalWrite(LEDREDPIN, !digitalRead(BTN1));
  Serial.print(analogRead(0)*(780.0/65536));
  for(uint8_t i=1; i<1+6; i++){
     Serial.print('\t');
     Serial.print(i);
     Serial.print(':');
     Serial.print(analogRead(i));
  }   
  for(uint8_t i=10; i<10+4; i++){
     Serial.print('\t');
     Serial.print(i);
     Serial.print(':');
     Serial.print(analogReadDiffADC10(i));
  }   
  for(uint8_t i=8; i<8+2; i++){
     Serial.print('\t');
     Serial.print(i);
     Serial.print(':');
     Serial.print(analogRead(i));
  }   
  for(uint8_t i=14; i<14+2; i++){
     Serial.print('\t');
     Serial.print(i);
     Serial.print(':');
     Serial.print(analogRead(i));
  }   

  Serial.println();
  delay(500);
  digitalWrite(LEDINFO0, !digitalRead(LEDINFO0));
  digitalWrite(LEDINFO1, !digitalRead(LEDINFO1));
  
  digitalWrite(EXT5, digitalRead(BTN0));
  digitalWrite(EXT6, digitalRead(BTN0));
  digitalWrite(EXT7, digitalRead(BTN0));
  digitalWrite(EXT8, digitalRead(BTN0));
  digitalWrite(EXT13, digitalRead(BTN0));

}

